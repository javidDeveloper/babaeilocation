package com.saeed.book;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.TypefaceSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;

import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.rd.PageIndicatorView;
import com.saeed.book.Adapter.GalleryPagerAdapter;
import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.CustomTypefaceSpan;
import com.saeed.book.lib.DataBase;
import com.saeed.book.lib.GifView;
import com.saeed.book.lib.TextViewPlus;
import com.saeed.book.lib.ToggleButton;
import com.saeed.book.lib.WrapContentViewPager;
//import com.xeoh.android.texthighlighter.TextHighlighter;
//import com.saeed.book.Adapter.;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.angmarch.views.NiceSpinner;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Target;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class ShowContent extends AppCompatActivity {

    public SharedPreferences shared;
    public SharedPreferences.Editor editor;
    public ImageView iv_favorites;
    public ImageView quicksettings;
    LinearLayout contentlinear;
    LinearLayout actionbar;
    LinearLayout sl;
    LinearLayout searchbar;
    TextViewPlus text_searchbar;
    ImageView close_searchbar;
    TextView title;
    int id;
    Intent intentsss;
    ImageView back_arrow;
    boolean iswebview;

    Typeface vazir;
    Typeface vazirb;
    Typeface shabnam;
    Typeface shabnamb;
    Typeface sahel;
    Typeface sahelb;

    Typeface MAINTF;
    Typeface MAINTFb;
    boolean fav_or_search = false;

    LayoutInflater inflater;

    //menu items
    NiceSpinner m_spinner;
    DiscreteSeekBar m_seek;
    SwitchCompat m_night_toggle;
    LinearLayout m_night;
    LinearLayout m_share;
    LinearLayout m_goup;
    LinearLayout m_go_down;

    NestedScrollView sc;
    WebView wv;
    int size;
    ImageView share;
    DataBase db;
    int api;
    LinearLayout.LayoutParams params;
    LinearLayout.LayoutParams paramsb;
    LinearLayout.LayoutParams paramslink;
    LinearLayout.LayoutParams paramsvideo;
    public String text_for_share = "";
    ArrayList<TextView> tvs;
    ArrayList<TextView> tvs_bold;
    ArrayList<Button> btns;
    public static ArrayList<VoicePlayer> vps;

    List<SeasonItem> items_in_this_season = new ArrayList<>();
    String arrenge;

    boolean intenthappen = false;
    boolean ress = false;
    static boolean showContentIsAlive = false;
    SeasonItem seasonItem;
    ProgressBar loading;

    ViewGroup videoLayout;
    View loadingView;

    LinearLayout jump_to_next_layout;
    LinearLayout next;
    LinearLayout nextl;
    LinearLayout prev;
    LinearLayout prevl;
    TextViewPlus prev_post_title;
    TextViewPlus next_post_title;
    int pos_in_list = 0;

    Context c;

    //private static final int PERMISSION_REQUEST_CODE = 1;

    public static int whichvideo;

    boolean aboutus = false;
    String search_in_text = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Settings_Activity.loadKeepOn(ShowContent.this)) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        if (!Settings_Activity.loadNightMode(this)) {
            setContentView(R.layout.show_content);
        } else {
            App.StatusColorDark(this);
            setContentView(R.layout.show_content_dark);
        }

        intenthappen = false;

        GetExtraIntents();

        c = ShowContent.this;

        InitView();

        loading.setVisibility(View.VISIBLE);

        if (!App.Hiding_Toolbar || aboutus) {
            CollapsingToolbarLayout toolbar = findViewById(R.id.collapsing_toolbar);
            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
            params.setScrollFlags(0);
        }

        //App.Loger("id geten: "+id);

        ress = App.restart_from_theme;
        final int WhereWasScroll = loadScrollc();
        int delay;
        if (Build.VERSION.SDK_INT > 19) {
            delay = 200;
        } else {
            delay = 300;
        }

        Log.e("saeeeed", WhereWasScroll + "");
        if (ress) {
            App.restart_from_theme = false;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sc.scrollTo(0, WhereWasScroll);
                }
            }, delay);

        }

        contentlinear.setVisibility(View.GONE);

        db = new DataBase(c);
        if(!aboutus){
            seasonItem = db.getFullContent(id);
        }

        if(!aboutus){
            Next_Previous_Jumper_Handler();
        }

        api = Build.VERSION.SDK_INT;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LoadContents();
            }
        }, 250);


        shared = getSharedPreferences("Prefs", MODE_PRIVATE);
        editor = shared.edit();

        size = Settings_Activity.loadFontSize(ShowContent.this);


        if(!App.InContentSettings){
            quicksettings.setVisibility(View.GONE);
        }

        if(aboutus){
            quicksettings.setVisibility(View.GONE);
        }

        quicksettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MenuDialog();

            }
        });

        if(!aboutus){
            FavHandler();
        }


    }


    private void LoadContents() {


        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 12, 0, 12);

        int valueInPixels = (int) getResources().getDimension(R.dimen.buttons_height);

        //actionbar.setBehaviorEnabled

        paramsb = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                valueInPixels);
        paramsb.setMargins(0, 12, 0, 12);

        paramslink = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                valueInPixels);
        paramslink.setMargins(1, 0, 1, 0);

        paramsvideo = new LinearLayout.LayoutParams(
                100,
                100);


        vazir = Typeface.createFromAsset(getAssets(), "fonts/vazir.ttf");
        vazirb = Typeface.createFromAsset(getAssets(), "fonts/vazirb.ttf");
        shabnam = Typeface.createFromAsset(getAssets(), "fonts/shabnam.ttf");
        shabnamb = Typeface.createFromAsset(getAssets(), "fonts/shabnamb.ttf");
        sahel = Typeface.createFromAsset(getAssets(), "fonts/sahel.ttf");
        sahelb = Typeface.createFromAsset(getAssets(), "fonts/sahelb.ttf");


        switch (Settings_Activity.loadFontType(ShowContent.this)) {
            case 0:
                MAINTF = vazir;
                MAINTFb = vazirb;
                break;
            case 1:
                MAINTF = shabnam;
                MAINTFb = shabnamb;
                break;
            case 2:
                MAINTF = sahel;
                MAINTFb = sahelb;
                break;
            default:
                MAINTF = vazir;
                MAINTFb = vazirb;
                break;
        }


        int whichvoice = 0;
        whichvideo = 0;


        String content;

        if(aboutus){
            content = db.getAboutUs();
            title.setText(getResources().getString(R.string.aboutus));
        }else{
            content = seasonItem.getSeasonContent();
            String stringtitle = seasonItem.getSeasonTitle();
            title.setText(stringtitle);

        }

        if (content.startsWith("code==")) {


            content = decryptIt(content, c);


        }

        if(content.startsWith("webview==")){

            iswebview = true;

            sc.setVisibility(View.GONE);
            wv.setVisibility(View.VISIBLE);

            if(App.isOnline(c)){

                content = content.replace("\r\n", "").replace("\n", "").replace(" ", "");
                String code[] = content.split("==");

                String url = code[1];
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                final String url2 = url;

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wv.loadUrl(url2);
                        wv.getSettings().setJavaScriptEnabled(true);
                        EnableInternalLinks();
                    }
                }, 1000);




            }else{
                App.ToastMaker(c,"عدم دسترسی به اینترنت!");
            }


        }else{

            iswebview = false;
            sc.setVisibility(View.VISIBLE);
            wv.setVisibility(View.GONE);

            String[] contentarray = content.split("\\!\\#");

            for (String part : contentarray) {

                if (part.contains("onlineimg==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._image, null);

                    ImageView image = view.findViewById(R.id.image);
                    final TextView cantload = view.findViewById(R.id.cantload);

                    Glide.with(c)
                            .load(code[1]).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                            cantload.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(image);

                    contentlinear.addView(view);

                } else if (part.contains("onlineimgfit==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._fit_image, null);

                    ImageView image = view.findViewById(R.id.image);
                    final TextView cantload = view.findViewById(R.id.cantload);

                    Glide.with(c)
                            .load(code[1]).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                            cantload.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(image);

                    contentlinear.addView(view);

                } else if (part.contains("img==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._image, null);

                    ImageView image = view.findViewById(R.id.image);

                    int resID = getResources().getIdentifier(formatremover(code[1]), "mipmap", getPackageName());
                    image.setImageResource(resID);

                    contentlinear.addView(view);

                }  else if (part.contains("imgfit==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._fit_image, null);

                    ImageView image = view.findViewById(R.id.image);

                    int resID = getResources().getIdentifier(formatremover(code[1]), "mipmap", getPackageName());
                    image.setImageResource(resID);

                    contentlinear.addView(view);

                } else if (part.contains("link==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._button, null);

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button blink = view.findViewById(R.id.btn);

                    blink.setText(code[1]);
                    blink.setTextColor(getResources().getColor(R.color.text_light));

                    blink.setTextSize(size+1);
                    blink.setTypeface(MAINTFb);

                    btns.add(blink);

                    blink.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String url = code[2].trim();
                            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                url = "http://" + url;
                            }
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("dialog==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._button, null);

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button btn = view.findViewById(R.id.btn);

                    btn.setText(code[1]);
                    btn.setTextColor(getResources().getColor(R.color.text_light));

                    btn.setTextSize(size+1);
                    btn.setTypeface(MAINTFb);

                    btns.add(btn);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            App.DialogMaker(c,code[2],code[3]);
                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("call==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view;

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button btn = view.findViewById(R.id.btn);

                    btn.setText(code[1]);
                    btn.setTextColor(getResources().getColor(R.color.text_light));

                    btn.setTextSize(size+1);
                    btn.setTypeface(MAINTFb);

                    btns.add(btn);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try{
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + code[2]));
                                startActivity(intent);
                            }catch (Exception e){
                                App.ToastMaker(c,"به نظر میرسد دستگاه از تماس پشتیبانی نمیکند");
                            }

                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("sms==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._button, null);

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button btn = view.findViewById(R.id.btn);

                    btn.setText(code[1]);
                    btn.setTextColor(getResources().getColor(R.color.text_light));

                    btn.setTextSize(size+1);
                    btn.setTypeface(MAINTFb);

                    btns.add(btn);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            sendIntent.setData(Uri.parse("sms:"));
                            sendIntent.putExtra("address", new String (code[2]));
                            try {
                                startActivity(sendIntent);
                            }
                            catch(Exception e) {
                                Toast.makeText(c,"اپ پیامک یافت نشد",Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("email==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._button, null);

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button btn = view.findViewById(R.id.btn);

                    btn.setText(code[1]);
                    btn.setTextColor(getResources().getColor(R.color.text_light));

                    btn.setTextSize(size+1);
                    btn.setTypeface(MAINTFb);

                    btns.add(btn);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.parse("mailto:"));
                            intent.putExtra(Intent.EXTRA_EMAIL  , new String[] { code[2] });
                            startActivity(Intent.createChooser(intent, "Email via..."));
                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("jumpto==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._button, null);

                    if(Settings_Activity.loadNightMode(c)){
                        view = inflater.inflate(R.layout._button_dark, null);
                    }else{
                        view = inflater.inflate(R.layout._button, null);
                    }

                    Button btn = view.findViewById(R.id.btn);

                    btn.setText(code[1]);
                    btn.setTextColor(getResources().getColor(R.color.text_light));

                    btn.setTextSize(size+1);
                    btn.setTypeface(MAINTFb);

                    btns.add(btn);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(getApplicationContext(), ShowContent.class);
                            int idz;
                            try{
                                idz = Integer.parseInt(code[2]);
                                i.putExtra("id", idz);
                                startActivity(i);
                            }catch (Exception e){
                                App.ToastMaker(c,"آیدی وارد شده در دیتابیس اشتباه است");
                            }

                        }
                    });

                    contentlinear.addView(view);

                } else if (part.contains("onlineimgzoom==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._image, null);

                    ImageView image = view.findViewById(R.id.image);
                    final TextView cantload = view.findViewById(R.id.cantload);

                    Glide.with(c)
                            .load(code[1]).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                            cantload.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(image);

                    Zoomy.Builder builder = new Zoomy.Builder(this).target(image);
                    builder.register();

                    contentlinear.addView(view);


                }else if (part.contains("onlineimgzoomfit==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._fit_image, null);

                    ImageView image = view.findViewById(R.id.image);
                    final TextView cantload = view.findViewById(R.id.cantload);

                    Glide.with(c)
                            .load(code[1]).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                            cantload.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(image);

                    Zoomy.Builder builder = new Zoomy.Builder(this).target(image);
                    builder.register();

                    contentlinear.addView(view);

                }else if (part.contains("galleryview==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._fit_image_gallery, null);

                    int size = code.length - 1;



                    List<String> urls = new ArrayList<>();
                    for (int i = 0 ; i<size ; i++){
                        urls.add(code[i+1]);
                    }

                    App.Loger("size == " + size);

                    GalleryPagerAdapter adapter = new GalleryPagerAdapter(c,urls);

                    final WrapContentViewPager mViewPager = view.findViewById(R.id.viewpager);

                    mViewPager.setAdapter(adapter);

                    PageIndicatorView pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
                    pageIndicatorView.setCount(size);

                    pageIndicatorView.setViewPager(mViewPager);

                    contentlinear.addView(view);

                } else if (part.contains("imgzoomfit==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._fit_image, null);

                    ImageView image = view.findViewById(R.id.image);

                    int resID = getResources().getIdentifier(formatremover(code[1]), "mipmap", getPackageName());
                    image.setImageResource(resID);

                    Zoomy.Builder builder = new Zoomy.Builder(this).target(image);
                    builder.register();

                    contentlinear.addView(view);

                } else if (part.contains("imgzoom==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._image, null);

                    ImageView image = view.findViewById(R.id.image);

                    int resID = getResources().getIdentifier(formatremover(code[1]), "mipmap", getPackageName());
                    image.setImageResource(resID);

                    Zoomy.Builder builder = new Zoomy.Builder(this).target(image);
                    builder.register();

                    contentlinear.addView(view);

                } else if (part.contains("offlinevideo==")) {


                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    final String code[] = part.split("==");
                    final int vv = whichvideo;

                    ImageView vidprev = new ImageView(c);
                    vidprev.setImageResource(R.drawable.video_preview);
                    vidprev.setVisibility(View.VISIBLE);
                    vidprev.setLayoutParams(params);
                    vidprev.setAdjustViewBounds(true);
                    vidprev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            tt = sc.getScrollY();
                            saveScrollc(tt);
                            Intent g = new Intent(ShowContent.this, PlayVideo.class);
                            g.putExtra("id", id);
                            g.putExtra("whichvid", vv);
                            g.putExtra("code2", formatremover(code[1]));
                            g.putExtra("code", "");
                            try {
                                if (intentsss.getStringExtra("from").equalsIgnoreCase("Fav")) {
                                    g.putExtra("from", "Fav");
                                }
                            } catch (Exception e) {
                            }
                            startActivity(g);
                        }
                    });
                    contentlinear.addView(vidprev);
                    whichvideo++;

                } else if (part.contains("video==")) {

                    final int vv = whichvideo;
                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    final String code[] = part.split("==");


                    ImageView vidprev = new ImageView(c);
                    vidprev.setImageResource(R.drawable.video_preview);
                    vidprev.setVisibility(View.VISIBLE);
                    vidprev.setAdjustViewBounds(true);
                    vidprev.setLayoutParams(params);
                    if (!code[1].startsWith("http://") && !code[1].startsWith("https://")) {
                        code[1] = "http://" + code[1];
                    }
                    vidprev.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            tt = sc.getScrollY();
                            saveScrollc(tt);
                            Intent g = new Intent(ShowContent.this, PlayVideo.class);
                            g.putExtra("id", id);
                            g.putExtra("whichvid", vv);
                            g.putExtra("code", code[1]);
                            g.putExtra("code2", "");
                            try {
                                if (intentsss.getStringExtra("from").equalsIgnoreCase("Fav")) {
                                    g.putExtra("from", "Fav");
                                }
                            } catch (Exception e) {
                            }
                            startActivity(g);
                        }
                    });
                    contentlinear.addView(vidprev);
                    whichvideo++;

                } else if (part.contains("offlinevoice==")) {

                    Log.w("show", "offline voice section started");
                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    final String code[] = part.split("==");
                    int resID = getResources().getIdentifier(formatremover(code[1]), "raw", getPackageName());
                    LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    VoicePlayer vp = new VoicePlayer(c);
                    vp.Initialize(false, null, resID, id, whichvoice);

                    contentlinear.addView(vp, para);
                    vps.add(vp);
                    whichvoice++;

                } else if (part.contains("voice==")) {
                    Log.w("show", "online voice section started");

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    final String code[] = part.split("==");

                    if (!code[1].startsWith("http://") && !code[1].startsWith("https://")) {
                        code[1] = "http://" + code[1];
                    }

                    Log.e("SSSSaeed>>>>>", "Voice url is: " + code[1]);

                    LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    VoicePlayer vp = new VoicePlayer(c);
                    vp.Initialize(true, code[1], 0, id, whichvoice);
                    contentlinear.addView(vp, para);
                    vps.add(vp);
                    whichvoice++;

                } else if (part.contains("gif==")) {

                    part = part.replace("\r\n", "").replace("\n", "").replace(" ", "");
                    String code[] = part.split("==");
                    int resID = getResources().getIdentifier(formatremover(code[1]), "mipmap", getPackageName());

                    GifView gif = new GifView(c);
                    gif.setMovieResource(resID);
                    gif.setVisibility(View.VISIBLE);
                    gif.setLayoutParams(params);

                    contentlinear.addView(gif);

                }  else if (part.contains("quote==")) {

                    part = part.trim();
                    final String code[] = part.split("==");

                    View view;
                    if (!Settings_Activity.loadNightMode(c)) {
                        view = inflater.inflate(R.layout._quote, null);
                    } else {
                        view = inflater.inflate(R.layout._quote_dark, null);
                    }
                    TextView title = view.findViewById(R.id.title);
                    TextView text = view.findViewById(R.id.text);

                    if (App.CopyText) {
                        title.setTextIsSelectable(true);
                        text.setTextIsSelectable(true);
                    } else {
                        title.setTextIsSelectable(false);
                        text.setTextIsSelectable(false);
                    }

                    title.setText(code[1]);
                    text.setText(code[2]);

                    title.setTypeface(MAINTFb);
                    text.setTypeface(MAINTF);

                    title.setTextSize(size+3);
                    text.setTextSize(size);
                    tvs_bold.add(title);
                    tvs.add(text);

                    contentlinear.addView(view);

                } else if (part.contains("divider==")) {

                    part = part.trim();
                    final String code[] = part.split("==");

                    View dividerview;

                    if (code[1].equals("1")) {
                        dividerview = inflater.inflate(R.layout._divider, null);
                    }else{
                        dividerview = inflater.inflate(R.layout._divider2, null);
                    }

                    contentlinear.addView(dividerview);

                } else if (part.contains("title==")) {

                    part = part.replace("\r\n", "").replace("\n", "");
                    final String code[] = part.split("==");

                    View view = inflater.inflate(R.layout._text, null);

                    TextView tv = null;
                    tv = view.findViewById(R.id.text);
                    code[1] = code[1].trim();



                    if (code[1].startsWith("[color:")) {

                        String colorcode = code[1].substring(0, 14);
                        String color[] = colorcode.split(":");
                        String fina = color[1].replace("]", "");
                        code[1] = code[1].substring(14);
                        tv.setTextColor(Color.parseColor("#" + fina.trim()));

                    }else{

                        if (!Settings_Activity.loadNightMode(c)) {
                            tv.setTextColor(c.getResources().getColor(R.color.text_dark));
                        } else {
                            tv.setTextColor(c.getResources().getColor(R.color.text_light));
                        }

                    }


                    if(code[1].length()>0){

                        tv.setText(code[1]);
                        tv.setTextSize(size+4);
                        tv.setTypeface(MAINTFb);
                        tv.setLineSpacing(App.SpaceBetweenLines, 1);

                        tvs_bold.add(tv);

                        contentlinear.addView(view);
                    }


                } else {


                    View view = inflater.inflate(R.layout._text, null);
                    TextView tv = view.findViewById(R.id.text);

                    if (App.CopyText) {
                        tv.setTextIsSelectable(true);
                    } else {
                        tv.setTextIsSelectable(false);
                    }

                    part = part.trim();

                    if(part.length()>0){
                        if (!Settings_Activity.loadNightMode(c)) {
                            tv.setTextColor(c.getResources().getColor(R.color.text_dark));
                        } else {
                            tv.setTextColor(c.getResources().getColor(R.color.text_light));
                        }

                        boolean flag_bold = false;

                        for (int i = 0; i < 3; i++) {
                            if (part.startsWith("[left]")) {
                                part = part.replace("[left]", "");
                                tv.setGravity(Gravity.LEFT);
								part = part.trim();
                            } else if (part.startsWith("[center]")) {
                                part = part.replace("[center]", "");
                                tv.setGravity(Gravity.CENTER);
								part = part.trim();
                            } else if (part.startsWith("[right]")) {
                                part = part.replace("[right]", "");
                                tv.setGravity(Gravity.RIGHT);
								part = part.trim();
                            } else if (part.startsWith("[color:")) {
                                String colorcode = part.substring(0, 14);
                                String color[] = colorcode.split(":");
                                String fina = color[1].replace("]", "");
                                part = part.substring(14);
                                tv.setTextColor(Color.parseColor("#" + fina.trim()));
								part = part.trim();
                            }else if (part.startsWith("[bold]")) {
                                part = part.replace("[bold]", "");
                                flag_bold = true;
								part = part.trim();
                            }
                        }

                        part = part.trim();

                        if(part.contains("[b]")){

                            int startbold = part.indexOf("[b]");
                            int endbold = part.lastIndexOf("[b]")-3;

                            part = part.replaceAll("\\[b\\]" ,"");

                            TypefaceSpan MAINTF_Span = new CustomTypefaceSpan("", MAINTF);
                            TypefaceSpan MAINTF_b_Span = new CustomTypefaceSpan("", MAINTFb);

                            SpannableStringBuilder str = new SpannableStringBuilder(part);

                            str.setSpan(MAINTF_Span, 0, startbold, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                            str.setSpan(MAINTF_b_Span, startbold, endbold, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                            str.setSpan(MAINTF_Span, endbold, part.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);


                            tv.setText(str);

                            text_for_share = text_for_share + part + "\n";
                            tv.setTextSize(size);
                            tv.setTypeface(MAINTF);
                            tv.setLineSpacing(App.SpaceBetweenLines, 1);

                        }else{
                            tv.setText(part);
                            text_for_share = text_for_share + part + "\n";
                            tv.setTextSize(size);

                            if(flag_bold){
                                tv.setTypeface(MAINTFb);
                            }else{
                                tv.setTypeface(MAINTF);
                            }
                            tv.setLineSpacing(App.SpaceBetweenLines, 1);
                        }


                        contentlinear.addView(view);
                        tvs.add(tv);
                        if(flag_bold){
                            tvs_bold.add(tv);
                        }else{
                            tvs.add(tv);
                        }
                    }

                }

            }

        }

        loading.setVisibility(View.GONE);
        contentlinear.setVisibility(View.VISIBLE);

        if (!ress) {
            final int WhereWasScroll = loadScrollc();
            int delay;
            if (Build.VERSION.SDK_INT > 19) {
                delay = 200;
            } else {
                delay = 300;
            }
            if (App.SaveReadingPosition) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //App.Loger("shit2");
                        ObjectAnimator.ofInt(sc, "scrollY", WhereWasScroll).setDuration(500).start();
                    }
                }, delay);
            }
        }


        if(App.Jump_To_Next_Or_Previous && items_in_this_season.size()>1 && !fav_or_search && !aboutus){
            jump_to_next_layout.setVisibility(View.VISIBLE);
        }else{
            jump_to_next_layout.setVisibility(View.INVISIBLE);
        }

        if(!search_in_text.equals("")){
            searchbar.setVisibility(View.VISIBLE);
            text_searchbar.setText("جستجو " + "\"" + search_in_text + "\"" + " در این مطلب");
        }

        if(!search_in_text.equals("")){

            for(int i = 0 ; i<tvs.size() ; i++){
                HighLightHandler_tvs(i,search_in_text);
            }

            for(int i = 0 ; i<tvs_bold.size() ; i++){
                HighLightHandler_tvs_bold(i,search_in_text);
            }

        }


    }


    private void HighLightHandler_tvs(int pos,String tx){

        if (tvs.get(pos).getText().toString().contains(tx)) {

//            TextHighlighter textHighlighter = new TextHighlighter();
//
//            if (!Settings_Activity.loadNightMode(c)) {
//                textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//            } else {
//                textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//            }
//            textHighlighter.setBold(false);
//            textHighlighter.setItalic(false);
//            textHighlighter.addTarget(tvs.get(pos));
//            textHighlighter.highlight(tx, TextHighlighter.BASE_MATCHER);

            highlightString(tvs.get(pos),tx);


        } else {

            //TextHighlighter textHighlighter = new TextHighlighter();
            String parts[] = tx.split(" ");
            for (int i = 0; i < parts.length; i++) {

                if(parts[i].length()>1){
//
//                    if (!Settings_Activity.loadNightMode(c)) {
//                        textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//                    } else {
//                        textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//                    }
//                    textHighlighter.setBold(false);
//                    textHighlighter.setItalic(false);
//                    textHighlighter.addTarget(tvs.get(pos));
//                    textHighlighter.highlight(parts[i], TextHighlighter.BASE_MATCHER);

                    highlightString(tvs.get(pos),parts[i]);

                }

            }
        }

    }

    private void HighLightHandler_tvs_bold(int pos,String tx){

        if (tvs_bold.get(pos).getText().toString().contains(tx)) {

//            TextHighlighter textHighlighter = new TextHighlighter();
//
//            if (!Settings_Activity.loadNightMode(c)) {
//                textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//            } else {
//                textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//            }
//            textHighlighter.setBold(false);
//            textHighlighter.setItalic(false);
//            textHighlighter.addTarget(tvs_bold.get(pos));
//            textHighlighter.highlight(tx, TextHighlighter.BASE_MATCHER);

            highlightString(tvs_bold.get(pos),tx);


        } else {

            //TextHighlighter textHighlighter = new TextHighlighter();
            String parts[] = tx.split(" ");
            for (int i = 0; i < parts.length; i++) {

                if(parts[i].length()>1){

//                    if (!Settings_Activity.loadNightMode(c)) {
//                        textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//                    } else {
//                        textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//                    }
//                    textHighlighter.setBold(false);
//                    textHighlighter.setItalic(false);
//                    textHighlighter.addTarget(tvs_bold.get(pos));
//                    textHighlighter.highlight(parts[i], TextHighlighter.BASE_MATCHER);

                    highlightString(tvs_bold.get(pos),parts[i]);

                }

            }
        }

    }



    private void highlightString(TextView tv,String input) {

        SpannableString spannableString = new SpannableString(tv.getText());
//        BackgroundColorSpan[] backgroundSpans = spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
//
//        for (BackgroundColorSpan span: backgroundSpans) {
//            spannableString.removeSpan(span);
//        }

        int color;
        if (!Settings_Activity.loadNightMode(c)) {
            color = c.getResources().getColor(R.color.search_highlight_color);
        } else {
            color = c.getResources().getColor(R.color.search_highlight_color_dark);
        }

        int indexOfKeyword = spannableString.toString().indexOf(input);

        while (indexOfKeyword > -1) {
            //Create a background color span on the keyword
            spannableString.setSpan(new BackgroundColorSpan(color), indexOfKeyword, indexOfKeyword + input.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //Get the next index of the keyword
            indexOfKeyword = spannableString.toString().indexOf(input, indexOfKeyword + input.length());
        }

        tv.setText(spannableString);

    }

    private void highlightRemover(TextView tv) {

        SpannableString spannableString = new SpannableString(tv.getText());
        BackgroundColorSpan[] backgroundSpans = spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);

        for (BackgroundColorSpan span: backgroundSpans) {
            spannableString.removeSpan(span);
        }

        tv.setText(spannableString);

    }


    private boolean isFirstTime() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore2", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore2", true);
            editor.commit();
        }
        return !ranBefore;
    }

    private void InitView() {

        inflater = this.getLayoutInflater();
        tvs = new ArrayList<TextView>();
        tvs_bold = new ArrayList<TextView>();
        btns = new ArrayList<Button>();
        vps = new ArrayList<VoicePlayer>();

        loadingView = getLayoutInflater().inflate(R.layout.view_loading_video, null);
        sc = (NestedScrollView) findViewById(R.id.sc);
        wv = (WebView) findViewById(R.id.wv);
        title = (TextView) findViewById(R.id.title_bar);
        sl = (LinearLayout) findViewById(R.id.sl);

        searchbar = findViewById(R.id.searchbar);
        text_searchbar = findViewById(R.id.text_searchbar);
        close_searchbar = findViewById(R.id.close_searchbar);

        loading = (ProgressBar) findViewById(R.id.loading);
        share = (ImageView) findViewById(R.id.share);
        contentlinear = (LinearLayout) findViewById(R.id.contentlinear);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);

        quicksettings = (ImageView) findViewById(R.id.quicksettings);
        actionbar = (LinearLayout) findViewById(R.id.actionbar);

        jump_to_next_layout = findViewById(R.id.jump_to_next_layout);
        next = findViewById(R.id.next);
        nextl = findViewById(R.id.nextl);
        prev = findViewById(R.id.prev);
        prevl = findViewById(R.id.prevl);
        prev_post_title = findViewById(R.id.prev_post_title);
        next_post_title = findViewById(R.id.next_post_title);


        jump_to_next_layout.setVisibility(View.INVISIBLE);

        if(aboutus){
            back_arrow.setVisibility(View.VISIBLE);
        }

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        close_searchbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0 ; i<tvs.size() ; i++){
                    //HighLightHandler_tvs(i,"_lorem_txt__");
                    highlightRemover(tvs.get(i));
                }

                for(int i = 0 ; i<tvs_bold.size() ; i++){
                    //HighLightHandler_tvs_bold(i,"_lorem_txt__");
                    highlightRemover(tvs_bold.get(i));
                }
                searchbar.setVisibility(View.GONE);
            }
        });

    }

    private void FavHandler(){

        iv_favorites = (ImageView) findViewById(R.id.bookmark_bar);
        iv_favorites.setVisibility(View.INVISIBLE);

        if (db.isFav(id)) {
            iv_favorites.setImageDrawable(getResources().getDrawable(R.drawable.bookmarked));
        } else {
            iv_favorites.setImageDrawable(getResources().getDrawable(R.drawable.not_bookmarked));
        }

        iv_favorites.setVisibility(View.VISIBLE);


        iv_favorites.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (db.isFav(id)) {

                    db.removeFav(id);
                    iv_favorites.setImageResource(R.drawable.not_bookmarked);
                    // show message
                    String message = getResources().getString(R.string.remove_fav);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } else {

                    db.addToFav(id);
                    iv_favorites.setImageResource(R.drawable.bookmarked);
                    // show message
                    String message = getResources().getString(R.string.addto_fav);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                }

            }
        });

        if(!App.FavList){
            iv_favorites.setVisibility(View.GONE);
        }

        if(aboutus){
            iv_favorites.setVisibility(View.GONE);
        }

    }

    int tt;

    public void restart(final NestedScrollView sv) {



        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {


                App.restart_from_theme = true;
                tt = sv.getScrollY();
                intenthappen = true;
                saveScrollc(tt);

                ShowContent.this.recreate();


            }
        }, 200);



    }

    private int loadScrollc() {
        String tag = id + "";
        SharedPreferences sp = getSharedPreferences("C" + tag, Activity.MODE_PRIVATE);
        return sp.getInt("C" + tag, 0);
    }

    private void saveScrollc(int value) {
        String tag = id + "";
        SharedPreferences sp = getSharedPreferences("C" + tag, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("C" + tag, value);
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tt = sc.getScrollY();
        saveScrollc(tt);
    }

    @Override
    protected void onPause() {
        super.onPause();

        App.HandlerPauseBackgroundMusic(intenthappen,this);


        for (int i = 0; i < vps.size(); i++) {
            if (vps.get(i).isPlaying()) {
                vps.get(i).pause();
            }
            if (App.SaveVoiceSeekPosition) {
                saveVoice(id, i, vps.get(i).getCurrentPosition(), ShowContent.this);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        App.HandlerResumeBackgroundMusic(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showContentIsAlive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        showContentIsAlive = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static void saveVoice(int id, int Number, int value, Context c) {
        String TAG = "voice" + id + "-" + Number;
        Log.e("saved tag >>>>>>>>", TAG + "and pos:" + value);
        SharedPreferences sp = c.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(TAG, value);
        editor.commit();
    }

    public static int loadVoice(int id, int Number, Context c) {
        String TAG = "voice" + id + "-" + Number;
        SharedPreferences sp = c.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
        return sp.getInt(TAG, 0);
    }

    public static String decryptIt(String value, Context c) {

        App.Loger("Decrypt Started ...");
        String coded = value.substring(6, value.length()).trim();
        //App.Loger(coded);
        //App.Loger(coded.length() + " length");

        String result = null;

        try {
            // Decoding base64
            byte[] bytesDecoded = Base64.decode(coded.getBytes("UTF-8"), Base64.DEFAULT);

            SecretKeySpec key = new SecretKeySpec(MainActivity.Key.getBytes(), "DES");

            Cipher cipher = Cipher.getInstance("DES/ECB/ZeroBytePadding");

            // Initialize the cipher for decryption
            cipher.init(Cipher.DECRYPT_MODE, key);

            // Decrypt the text
            byte[] textDecrypted = cipher.doFinal(bytesDecoded);

            result = new String(textDecrypted);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (BadPaddingException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        } catch (Exception e) {
            e.printStackTrace();
            App.DialogMaker(c, "خطای رمزگشایی", "مشکلی در رمزگشایی بوجود آمد:" + "\n" + e.getMessage());
            return "Decrypt Error";
        }

        App.Loger("Decrypt Finished ...");
        return result;
    }

    AlertDialog alert;
    public void MenuDialog() {

        alert = new AlertDialog.Builder(c).create();

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView;

        if (!Settings_Activity.loadNightMode(this)) {
            dialogView = inflater.inflate(R.layout.showcontent_menu_dialog, null);
        } else {
            dialogView = inflater.inflate(R.layout.showcontent_menu_dialog_night, null);
        }

        alert.setView(dialogView);

        m_spinner = (NiceSpinner) dialogView.findViewById(R.id.m_spinner);
        m_seek = (DiscreteSeekBar) dialogView.findViewById(R.id.m_seek);
        m_night_toggle = (SwitchCompat) dialogView.findViewById(R.id.m_night_toggle);
        m_night = (LinearLayout) dialogView.findViewById(R.id.m_night);
        m_share = (LinearLayout) dialogView.findViewById(R.id.m_share);
        m_goup = (LinearLayout) dialogView.findViewById(R.id.m_goup);
        m_go_down = (LinearLayout) dialogView.findViewById(R.id.m_godown);

        m_seek.setMin(App.Min_Font_Size);
        m_seek.setMax(App.Max_Font_Size);

        if (Settings_Activity.loadNightMode(ShowContent.this)) {
            m_night_toggle.setChecked(true);
        } else {
            m_night_toggle.setChecked(false);
        }

        if (!App.Change_Day_Night_Mode_Settings) {
            m_night.setVisibility(View.GONE);
        }

        m_night.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Settings_Activity.loadNightMode(ShowContent.this)) {
                    m_night_toggle.setChecked(false);
                    Settings_Activity.saveNightMode(false, ShowContent.this);
                    restart(sc);

                } else {

                    m_night_toggle.setChecked(true);
                    Settings_Activity.saveNightMode(true, ShowContent.this);
                    restart(sc);
                }
            }
        });

        m_night_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    Settings_Activity.saveNightMode(false, ShowContent.this);
                    restart(sc);
                } else {
                    Settings_Activity.saveNightMode(true, ShowContent.this);
                    restart(sc);
                    restart(sc);
                }
            }
        });


        List<String> dataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.font_selector)));
        m_spinner.attachDataSource(dataset);

        m_spinner.setSelectedIndex(Settings_Activity.loadFontType(ShowContent.this));
        m_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Settings_Activity.saveFontType(position, ShowContent.this);
                switch (position) {
                    case 0:
                        MAINTF = vazir;
                        MAINTFb = vazirb;
                        break;
                    case 1:
                        MAINTF = shabnam;
                        MAINTFb = shabnamb;
                        break;
                    case 2:
                        MAINTF = sahel;
                        MAINTFb = sahelb;
                        break;
                    default:
                        MAINTF = vazir;
                        MAINTFb = vazirb;
                        break;
                }

                for (int i = 0; i < tvs.size(); i++) {
                    tvs.get(i).setTypeface(MAINTF);
                    tvs.get(i).setLineSpacing(App.SpaceBetweenLines, 1);
                }

                for (int i = 0; i < tvs_bold.size(); i++) {
                    tvs_bold.get(i).setTypeface(MAINTFb);
                    tvs_bold.get(i).setLineSpacing(App.SpaceBetweenLines, 1);
                }

                for (int i = 0; i < btns.size(); i++) {
                    btns.get(i).setTypeface(MAINTFb);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!Settings_Activity.loadNightMode(this)) {
            m_spinner.setTextColor(Color.parseColor("#202020"));
        } else {
            m_spinner.setTextColor(Color.parseColor("#ffffff"));
        }

        m_seek.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                size = value;
                Settings_Activity.saveFontSize(size, ShowContent.this);
                for (int i = 0; i < tvs.size(); i++) {
                    tvs.get(i).setTextSize(size);
                }
                for (int i = 0; i < tvs_bold.size(); i++) {
                    tvs_bold.get(i).setTextSize(size+4);
                }
                for (int i = 0; i < btns.size(); i++) {
                    btns.get(i).setTextSize(size+2);
                }
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
            }
        });

        m_seek.setProgress(size);

        if (!App.Share) {
            m_share.setVisibility(View.GONE);
        }

        m_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert.dismiss();

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_for_share);
                startActivity(Intent.createChooser(sharingIntent, "اشتراک گذاری"));
            }
        });

        if(!App.Jumpers){
            m_go_down.setVisibility(View.GONE);
            m_goup.setVisibility(View.GONE);
        }

        m_goup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.dismiss();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //sc.fullScroll(ScrollView.FOCUS_UP);
                        sc.fling(0);
                        sc.smoothScrollTo(0, 0);
                    }
                }, 300);
            }
        });

        m_go_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.dismiss();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sc.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                }, 300);
            }
        });


        alert.show();
    }


    private void Next_Previous_Jumper_Handler(){

        if(seasonItem.getSeasonArrenge().contains("-")){
            arrenge = seasonItem.getSeasonArrenge().substring(0,seasonItem.getSeasonArrenge().lastIndexOf("-"));
        }else{
            arrenge = "0";
        }

        items_in_this_season = db.getRowsWithData(arrenge);

        if(items_in_this_season.size()==1){
            jump_to_next_layout.setVisibility(View.INVISIBLE);
        }else{

            for (int i = 0 ; i<items_in_this_season.size() ; i++ ){
                if(items_in_this_season.get(i).getSeasonID() == seasonItem.getSeasonID()){
                    pos_in_list = i;
                    break;
                }
            }

            if(pos_in_list == 0){
                prevl.setVisibility(View.GONE);
                next_post_title.setText(items_in_this_season.get(1).getSeasonTitle());
            }else if(pos_in_list == items_in_this_season.size()-1){
                nextl.setVisibility(View.GONE);
                prev_post_title.setText(items_in_this_season.get(pos_in_list-1).getSeasonTitle());
            }else{
                prev_post_title.setText(items_in_this_season.get(pos_in_list-1).getSeasonTitle());
                next_post_title.setText(items_in_this_season.get(pos_in_list+1).getSeasonTitle());
            }




            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    intenthappen = true;
                    Intent i = new Intent(getApplicationContext(), ShowContent.class);
                    int id = items_in_this_season.get(pos_in_list+1).getSeasonID();
                    i.putExtra("id", id);
                    startActivity(i);
                    finish();
                }
            });

            prev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    intenthappen = true;
                    Intent i = new Intent(getApplicationContext(), ShowContent.class);
                    int id = items_in_this_season.get(pos_in_list-1).getSeasonID();
                    i.putExtra("id", id);
                    startActivity(i);
                    finish();
                }
            });
        }

    }

    private void GetExtraIntents(){
        intentsss = getIntent();
        id = intentsss.getIntExtra("id", 1);

        if(intentsss.hasExtra("fav_or_search")) {
            fav_or_search = intentsss.getBooleanExtra("fav_or_search",false);
        }else {
            fav_or_search = false;
        }

        if(intentsss.hasExtra("aboutus")) {
            aboutus = intentsss.getBooleanExtra("aboutus",false);
        }else {
            aboutus = false;
        }

        if(intentsss.hasExtra("search_in_text")) {
            search_in_text = intentsss.getStringExtra("search_in_text");
        }else {
            search_in_text = "";
        }



    }

    private void EnableInternalLinks(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(request.getUrl().toString());
                    loading.setVisibility(View.VISIBLE);
                    return false;
                }
            });
        }else {
            wv.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String request) {
                    view.loadUrl(request);
                    loading.setVisibility(View.VISIBLE);
                    return false;
                }

            });
        }

        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 75) {
                    loading.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(iswebview){
            if (wv.canGoBack()) {
                wv.goBack();
            } else {
                super.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }
    }
	
	
	
	public String formatremover(String withformat){

        String res = withformat;
        res = res.replace(".png","");
        res = res.replace(".jpg","");
        res = res.replace(".jpeg","");
        res = res.replace(".gif","");
        res = res.replace(".mp3","");
        res = res.replace(".mp4","");
        return res;
    }

    //    public void file_download(String uRl,String name) {
//        File direct = new File(Environment.getExternalStorageDirectory()
//                + "/"+ App.DownloadFolderName);
//
//        if (!direct.exists()) {
//            direct.mkdirs();
//        }
//
//        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
//
//        Uri downloadUri = Uri.parse(uRl);
//        DownloadManager.Request request = new DownloadManager.Request(
//                downloadUri);
//
//        request.setAllowedNetworkTypes(
//                DownloadManager.Request.NETWORK_WIFI
//                        | DownloadManager.Request.NETWORK_MOBILE)
//                .setAllowedOverRoaming(false).setTitle(name)
//                .setDescription("در حال دانلود تصویر")
//                .setDestinationInExternalPublicDir("/"+App.DownloadFolderName, name+".jpg");
//
//        mgr.enqueue(request);
//
//    }
//
//    private boolean checkPermission() {
//        int result = ContextCompat.checkSelfPermission(c, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (result == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    private void requestPermission() {
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)c, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            Toast.makeText(c, "برای ذخیره محتوا دسترسی حافظه لازم است", Toast.LENGTH_LONG).show();
//        } else {
//            ActivityCompat.requestPermissions((Activity)c , new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
//        }
//    }

}


//BookSource 4 - 2018 by ariansource.com
// in source faghat az tarigh hamin site forokhte misshavad
// dar sorati ke site digari in source ra beforosh miresand loffan etela dahid
//   batashakor. saeed.dc2011@gmail.com