package com.saeed.book.Managers;

import android.util.Log;
import android.widget.Toast;

import com.saeed.book.Models.ContextModel;
import com.saeed.book.Models.LoctionStatus;
import com.saeed.book.Models.StatusRest;
import com.saeed.book.Network.RestConnection;

import java.util.Observable;
import java.util.Observer;

/**
 * Developed by javid
 * Project : Weather_final-master
 */
public class NetworkManager extends Observable implements Observer {
    public static NetworkManager instance;
    private LoctionStatus oldLocationStatus;

    public NetworkManager() {
        RestConnection.getInstance().addObserver(this);
    }


    public LoctionStatus getOldLocationStatus() {
        return oldLocationStatus;
    }

    public void setOldLocationStatus(LoctionStatus oldLocationStatus) {
        this.oldLocationStatus = oldLocationStatus;
    }

    public static NetworkManager getInstance() {
        if (instance == null)
            instance = new NetworkManager();
        return instance;
    }

    public void callLocation(LoctionStatus loctionStatus) {
//        Toast.makeText(ContextModel.getContext(), "callLoc", Toast.LENGTH_SHORT).show();
        if (oldLocationStatus == null) {
            oldLocationStatus = loctionStatus;
            RestConnection.getInstance().callLocation(loctionStatus);
        } else if (loctionStatus.getLatitude().equals(oldLocationStatus.getLatitude())
                && loctionStatus.getLongitude().equals(oldLocationStatus.getLongitude())) {
        } else {
            RestConnection.getInstance().callLocation(loctionStatus);
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof RestConnection) {
            if (o instanceof StatusRest) {
                setChanged();
                notifyObservers(o);
            }
        }
    }
}
