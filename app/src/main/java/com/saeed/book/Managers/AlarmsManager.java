package com.saeed.book.Managers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import com.saeed.book.Models.ContextModel;
import com.saeed.book.Tools.AlarmReceiver;

/**
 * Developed by javid
 * Project : book
 */
public class AlarmsManager {
    private static AlarmsManager instance;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;


    public static AlarmsManager getInstance() {
        if (instance == null)
            instance = new AlarmsManager();
        return instance;
    }


    public void setAlarm() {
        alarmMgr = (AlarmManager) ContextModel.getContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ContextModel.getContext(), AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(ContextModel.getContext(), 0, intent, 0);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60 * 60 * 1000, alarmIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            alarmMgr.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60 * 60 * 1000, alarmIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60 * 60 * 1000, alarmIntent);

        }

    }

}
