package com.saeed.book.Managers;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.saeed.book.MainActivity;
import com.saeed.book.Models.ContextModel;
import com.saeed.book.Models.LoctionStatus;
import com.saeed.book.Models.StatusRest;
import com.saeed.book.Network.RestConnection;
import com.saeed.book.Tools.LocationService;

import java.util.Observable;
import java.util.Observer;

/**
 * Developed by javid
 * Project : book
 */
public class LocationsManager extends Observable implements Observer {
    private static LocationsManager instans;
    private static final String TAG = MainActivity.class.getSimpleName();
    private boolean mAlreadyStartedService = false;
    private LoctionStatus loctionStatus = new LoctionStatus();
    private TelephonyManager telephonyManager;

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    public static LocationsManager getInstans() {
        if (instans == null)
            instans = new LocationsManager();
        return instans;
    }

    public LocationsManager() {
        LocalBroadcastManager.getInstance(ContextModel.getContext()).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        telephonyManager = (TelephonyManager) ContextModel.getContext().getSystemService(Context.TELEPHONY_SERVICE);
                        String latitude = intent.getStringExtra(LocationService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(LocationService.EXTRA_LONGITUDE);
                        if (latitude.equals("") || latitude.equals("")) {
                            //////////////////////////////////////////////
                            loctionStatus.setLatitude(latitude);
                            loctionStatus.setLongitude(longitude);
                            if (ActivityCompat.checkSelfPermission(ContextModel.getContext(), Manifest.permission.READ_PHONE_STATE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                Log.d(TAG, "== Error On onConnected() Permission not granted");
                                ActivityCompat.requestPermissions(ContextModel.getCurrentActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE,},
                                        1500);
                                return;
                            }
                            String imei = telephonyManager.getDeviceId();
                            loctionStatus.setImeiCode(imei);
                            /////////////////////////////////////////////
                            NetworkManager.getInstance().callLocation(loctionStatus);
                        }
                        if (latitude != null && longitude != null) {
//                            Toast.makeText(context, " Latitude : " + latitude + "Longitude: " + longitude, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IntentFilter(LocationService.ACTION_LOCATION_BROADCAST)
        );
    }


    /**
     * Step 1: Check Google Play services
     */
    public void startStep1() {

        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {

            //Passing null to indicate that it is executing for the first time.
            startStep2(null);

        } else {
//            Toast.makeText(ContextModel.getContext(), "noplay", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ContextModel.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.

        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContextModel.getCurrentActivity());
        builder.setTitle("خطا در اتصال");
        builder.setMessage("لطفا اتصال خود را با اینترنت بررسی کنید");

        String positiveText = "تلاش مجدد";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        //Block the Application Execution until user grants the permissions
                        if (startStep2(dialog)) {

                            //Now make sure about location permission.
                            if (checkPermissions()) {

                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }

                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    public void startStep3() {
        if (!mAlreadyStartedService) {
//            Toast.makeText(ContextModel.getContext(), "در حال دریافت موقعیت...", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ContextModel.getContext(), LocationService.class);
            ContextModel.getContext().startService(intent);
            mAlreadyStartedService = true;
        }
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(ContextModel.getContext());
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(ContextModel.getCurrentActivity(), status, 2404).show();
            }
            return false;
        }
        return true;
    }


    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(ContextModel.getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(ContextModel.getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(ContextModel.getCurrentActivity()
                        ,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(ContextModel.getCurrentActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
//            showSnackbar(R.string.permission_rationale,
//                    android.R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
            // Request permission
            ActivityCompat.requestPermissions(ContextModel.getCurrentActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
//                        }
//                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ContextModel.getCurrentActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof RestConnection) {
            if (arg instanceof StatusRest) {
                setChanged();
                notifyObservers(arg);
            }
        }
    }
}
