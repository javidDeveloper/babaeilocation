package com.saeed.book;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;


public class Welcome extends AppCompatActivity {
    Intent intent;
    boolean intenthappen = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);



        if(App.FullScreenWelcome){
            setContentView(R.layout.welcome2);
        }else{
            setContentView(R.layout.welcome);
        }

        if(App.Start_With_Night_Mode){
            Settings_Activity.saveNightMode(true,Welcome.this);
        }

        intent = new Intent(Welcome.this,MainActivity.class);

        if(App.Music && Settings_Activity.loadMusic(Welcome.this)){
            startService(new Intent(Welcome.this,BackgroundSoundService.class));
        }else{
            stopService(new Intent(Welcome.this,BackgroundSoundService.class));
        }

        intenthappen = false;
        if(App.Welcome_Screen){
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    intenthappen =true;
                    startActivity(intent);
                    finish();

                }
            }, App.Welcome_Time);
        }else{
            intenthappen =true;
            startActivity(intent);
            finish();
        }



    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.HandlerPauseBackgroundMusic(intenthappen,this);
    }





}



//BookSource 4 by ariansource.com
// in source faghat az tarigh hamin site forokhte misshavad
// dar sorati ke site digari in source ra beforosh miresand loffan etela dahid
// taa az an site shekayat shavad. batashakor. saeed.dc2011@gmail.com