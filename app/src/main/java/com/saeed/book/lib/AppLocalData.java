package com.saeed.book.lib;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppLocalData {
    private final SharedPreferences mPrefs;

    public AppLocalData(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getData(String key) {

        return mPrefs.getString(key,"");
    }

    public void SaveData(String key, String data) {
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putString(key, data);
        mEditor.apply();
    }

}