//package com.saeed.book.lib;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.DatabaseUtils;
//
//
//import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
//import com.saeed.book.CustomVars.SeasonItem;
//
//
//import net.sqlcipher.database.SQLiteDatabase;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.List;
//
//public class DataBaseAssetCopier {
//
//    private static final String DATABASE_NAME = "book.db";
//    Context context;
//
//    public DataBaseAssetCopier(Context context) {
//        this.context = context;
//
//        try {
//            copyDataBase();
//        }catch (IOException e){
//            App.Loger(e.getMessage()+"");
//        }
//
//    }
//
//
//    private void copyDataBase() throws IOException {
//        InputStream myInput = context.getAssets().open("databases/" + DATABASE_NAME);
//        String outFileName = context.getDatabasePath(DATABASE_NAME) +"";
//        OutputStream myOutput = new FileOutputStream(outFileName);
//        byte[] buffer = new byte[1024];
//        int length;
//        while ((length = myInput.read(buffer)) > 0) {
//            myOutput.write(buffer, 0, length);
//        }
//
//        myOutput.flush();
//        myOutput.close();
//        myInput.close();
//        App.Loger("db copy done");
//    }
//
//
//}
