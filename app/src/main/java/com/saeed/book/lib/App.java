package com.saeed.book.lib;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.ablanco.zoomy.Zoomy;
import com.ablanco.zoomy.ZoomyConfig;
import com.danikula.videocache.HttpProxyCacheServer;
import com.saeed.book.Models.ContextModel;
import com.saeed.book.R;
import com.saeed.book.Settings_Activity;

import java.util.Locale;


public class App extends Application {

    // Your Info:

    public static String DeveloperEmail = "javiddeveloper@gmail.com" ;

    // You Can Change This Settings:

    public static int DataBase_Version = 5;
	
    public static int Min_Font_Size = 10 ;
    public static int Max_Font_Size = 27 ;
    public static int Defualt_Font = 1 ;   // 1=vazir  2=shabnam  3=sahel
    public static int Defualt_Font_Size = 16 ;
    public static boolean Rate_App = false ;
    public static int Welcome_Time = 2000 ; // 2000 = 2 s
    public static boolean Header = true ;
    public static boolean Share = true ;
    public static boolean Hiding_Toolbar = true ;
    public static boolean Jumpers = true ;
    public static boolean Welcome_Screen = true ;
    public static boolean CopyText = true ;
    public static boolean DoubleTapToExit = true ;
    public static boolean SaveReadingPosition = false ;
    public static boolean Music = true ;
    public static boolean Change_Day_Night_Mode_Settings = true ;
    public static boolean Start_With_Night_Mode = false ;
    public static boolean FullScreenWelcome = false ;
    public static boolean SaveVoiceSeekPosition = true ;
    public static boolean SaveVideoSeekPosition = true ;
    public static boolean NavigationDrawer = true ;
    public static boolean FirstRunMusic = false ;
    public static boolean ShowArrowForSeasons = true ;
    public static boolean FavList = true ;
    public static boolean InContentSettings = true ;
    public static boolean Jump_To_Next_Or_Previous = true ;
    public static boolean EnableGrid = false ;
    public static int     GridWidth = 180 ;
    public static boolean JumpToSeasons = false ;
    public static int     SpaceBetweenLines = 3 ;  // faseleye beyne khotot




    //End Of Settings. // Developed by: ariansource . com





    //Database Security
//    public static boolean SecureDatabase = false ;
//    public static int SeasonCount = 0 ;
//    public static int FirstSeasonCount = 0 ;



    //Don't Change this:!

    public static AppLocalData LocalData;

    public static String Saprator = "!___!!___!" ;

    public static Typeface vazir;
    public static Typeface vazirb;

    @Override
    public void onCreate() {
        super.onCreate();
        ContextModel.setContext(getApplicationContext());
        vazirb = Typeface.createFromAsset(getAssets(), "fonts/vazirb.ttf");
        vazir = Typeface.createFromAsset(getAssets(), "fonts/vazir.ttf");


        LocalData = new AppLocalData(this);

        Configuration newConfig  = new Configuration();
        newConfig.locale = Locale.ENGLISH;
        super.onConfigurationChanged(newConfig);
        Locale.setDefault(newConfig.locale);
        getBaseContext().getResources().updateConfiguration(newConfig, getResources().getDisplayMetrics());

        ZoomyConfig config = new ZoomyConfig();
        config.setImmersiveModeEnabled(false);
        Zoomy.setDefaultConfig(config);

    }

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        App app = (App) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }
	public static boolean fromrecent = false;
    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }

    public static void Loger (String m){
        Log.e("Saeed Tag",m);
    }

    public static void ToastMaker (Context c, String m){
        Toast.makeText(c, m, Toast.LENGTH_SHORT).show();
    }

    public static void DialogMaker (Context c,String title,String mes){
        AlertDialog.Builder alert = new AlertDialog.Builder(c);
        alert.setCancelable(true);
        alert.setTitle(title);
        alert.setMessage(mes);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alert.show();
    }

    public static boolean restart_from_theme = false ;
    public static int GetTitleImage (Context c , String rawarrenge){

        String arrenge = rawarrenge.replaceAll("-","_");

        String name = "t"+arrenge;
        int resnum = c.getResources().getIdentifier(name, "mipmap", c.getPackageName());
        if(resnum!=0){
            App.Loger("t"+arrenge + "   exact match");
            return resnum;
        }

        String newname = name;

        if(newname.contains("_")){
            App.Loger("l1 before: "+newname);
            newname = GetParentName(newname);
            App.Loger("l1 after: "+newname);
            int resnum2 = c.getResources().getIdentifier(newname, "mipmap", c.getPackageName());
            if(resnum2!=0){
                App.Loger("t"+arrenge + "   mother match " +newname);
                return resnum2;
            }
        }

        if(newname.contains("_")){
            App.Loger("l2 before: "+newname);
            newname = GetParentName(newname);
            App.Loger("l2 after: "+newname);
            int resnum2 = c.getResources().getIdentifier(newname, "mipmap", c.getPackageName());
            if(resnum2!=0){
                App.Loger("t"+arrenge + "   2level mother match " + newname);
                return resnum2;
            }
        }

        if(newname.contains("_")){
            App.Loger("l3 before: "+newname);
            newname = GetParentName(newname);
            App.Loger("l3 after: "+newname);
            int resnum2 = c.getResources().getIdentifier(newname, "mipmap", c.getPackageName());
            if(resnum2!=0){
                App.Loger("t"+arrenge + "   3level mother match "+ newname);
                return resnum2;
            }
        }

        App.Loger("t"+newname + "   no match");
        return 0;
    }

    public static String GetParentName(String arrenge){
        String parts[] = arrenge.split("_");
        String newname = "";
        for (int i = 0; i < parts.length - 1; i++) {

            if (i == parts.length - 2) {
                newname += parts[i];
            } else {
                newname += parts[i] + "_";
            }

        }
        return newname;
    }

    public static void HandlerResumeBackgroundMusic (Context c){
        try {
            if (App.Music && Settings_Activity.loadMusic(c)) {
                if (!BackgroundSoundService.player.isPlaying()) {
                    BackgroundSoundService.player.start();
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public static void HandlerPauseBackgroundMusic (boolean intenthappen, Context c){
        try {
            if (!intenthappen && App.Music && Settings_Activity.loadMusic(c)) {
                BackgroundSoundService.player.pause();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public static boolean isOnline(Context c) {
        ConnectivityManager cm =
                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / App.GridWidth);
        return noOfColumns;
    }

    public static void StatusColorDark (Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.night_maincolor_darker));
        }
    }

}
