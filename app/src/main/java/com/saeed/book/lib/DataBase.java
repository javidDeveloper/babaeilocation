package com.saeed.book.lib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.saeed.book.CustomVars.SeasonItem;

import java.util.ArrayList;
import java.util.List;

public class DataBase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "book.db";
    private static final int DATABASE_VERSION = App.DataBase_Version;
    SQLiteDatabase db;
    SQLiteQueryBuilder qb;
    Context contexts;

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade(DATABASE_VERSION);
        try {
            db = getReadableDatabase();
            qb = new SQLiteQueryBuilder();
        }catch (Exception e){
            App.ToastMaker(context,"ورژن دیتابیس را بالا نبرده اید! به آموزش های ویدیویی قسمت آپدیت مراجعه کنید");
        }
        contexts = context;

    }

    public List<SeasonItem> getRows(String arrenge) {

        if (arrenge.equals("0")) { //get the main seasons

            List<SeasonItem> list = new ArrayList<>();

            try {


                int count = 0;
                String query = "select count(*) from contenttb where level2='-' and level3='-' and level4='-' order by level1 asc" ;
                Cursor cc = db.rawQuery(query, null);
                cc.moveToFirst();
                count = cc.getInt(0);
                cc.close();



                String query2 = "select * from contenttb where level2='-' and level3='-' and level4='-' order by level1 asc";
                Cursor c = db.rawQuery(query2, null);

                //App.Loger("Data Count in DB: " + count);

                for (int i = 0; i < count; i++) {

                    c.moveToPosition(i);
                    //String arrengedb = c.getString(1);
                    String titledb = c.getString(5);
                    String contentdb = "";
                    if (c.getString(6) != null) {
                        if (c.getString(6).trim().length() < 1) {
                            contentdb = "";
                        } else {
                            contentdb = c.getString(6);
                        }
                    }

                    //App.Loger("Data added: " + c.getString(1));
                    SeasonItem si = new SeasonItem(c.getInt(0), c.getString(1), titledb, contentdb);


                    list.add(si);

                }


                c.close();
                return list;



            }catch (Exception e){

                App.ToastMaker(contexts,"ورژن دیتابیس را بالا ببرید یا یکبار برنامه را پاک کنید");
                return list;
            }

        } else {

            String parts[] = arrenge.split("-");

            int countpart = parts.length;

            String cond = "level1='1'";

            switch (countpart) {
                case 1:
                    cond = "level1 = '" + parts[0] + "' and level2 <> '-' and level3 = '-' and level4 = '-' order by level2 asc";
                    break;
                case 2:
                    cond = "level1 = '" + parts[0] + "' and level2 = '" + parts[1] + "' and level3 <> '-'  and level4 = '-' order by level3 asc";
                    break;
                case 3:
                    cond = "level1 = '" + parts[0] + "' and level2 = '" + parts[1] + "' and level3 = '" + parts[2] + "' and level4 <> '-' order by level4 asc";
                    break;
            }

            int count2 = 0;
            String query3 = "select count(*) from contenttb where " + cond;
            Cursor cc = db.rawQuery(query3, null);
            cc.moveToFirst();
            count2 = cc.getInt(0);
            cc.close();

            List<SeasonItem> list = new ArrayList<>();

            String query4 = "select * from contenttb where " + cond;
            Cursor c = db.rawQuery(query4, null);

            //App.Loger("Data Count in DB: " + count2);

            for (int i = 0; i < count2; i++) {

                c.moveToPosition(i);

                String arrengel1 = c.getString(1);
                String arrengel2 = c.getString(2);
                String arrengel3 = c.getString(3);
                String arrengel4 = c.getString(4);
                String titledb = c.getString(5);
                String contentdb = "";
                if (c.getString(6) != null) {
                    if (c.getString(6).trim().length() < 1) {
                        contentdb = "";
                    } else {
                        contentdb = c.getString(6);
                    }
                }

                String newarrenge = arrengel1;
                if (!arrengel2.equals("-")) {
                    newarrenge += "-" + arrengel2;
                }
                if (!arrengel3.equals("-")) {
                    newarrenge += "-" + arrengel3;
                }
                if (!arrengel4.equals("-")) {
                    newarrenge += "-" + arrengel4;
                }

                //App.Loger("Data added: " + newarrenge);
                SeasonItem si = new SeasonItem(c.getInt(0), newarrenge, titledb, contentdb);

                list.add(si);

            }


            c.close();
            return list;

        }


    }

    public List<SeasonItem> getRowsWithData(String arrenge) {

        if (arrenge.equals("0")) { //get the main seasons

            int count = 0;
            String query = "select count(*) from contenttb where level2='-' and level3='-' and level4='-' and content <> '' order by level1 asc";
            Cursor cc = db.rawQuery(query, null);
            cc.moveToFirst();
            count = cc.getInt(0);
            cc.close();

            List<SeasonItem> list = new ArrayList<>();

            String query2 = "select * from contenttb where level2='-' and level3='-' and level4='-'  and content <> '' order by level1 asc";
            Cursor c = db.rawQuery(query2, null);

            //App.Loger("Data Count in DB: " + count);

            for (int i = 0; i < count; i++) {

                c.moveToPosition(i);
                //String arrengedb = c.getString(1);
                String titledb = c.getString(5);
                String contentdb = "";
                if (c.getString(6) != null) {
                    if (c.getString(6).trim().length() < 1) {
                        contentdb = "";
                    } else {
                        contentdb = c.getString(6);
                    }
                }

                //App.Loger("Data added: " + c.getString(1));
                SeasonItem si = new SeasonItem(c.getInt(0), c.getString(1), titledb, contentdb);


                list.add(si);

            }


            c.close();
            return list;

        } else {

            String parts[] = arrenge.split("-");

            int countpart = parts.length;

            String cond = "level1='1'";

            switch (countpart) {
                case 1:
                    cond = "level1 = '" + parts[0] + "' and level2 <> '-' and level3 = '-' and level4 = '-' order by level2 asc";
                    break;
                case 2:
                    cond = "level1 = '" + parts[0] + "' and level2 = '" + parts[1] + "' and level3 <> '-'  and level4 = '-' order by level3 asc";
                    break;
                case 3:
                    cond = "level1 = '" + parts[0] + "' and level2 = '" + parts[1] + "' and level3 = '" + parts[2] + "' and level4 <> '-' order by level4 asc";
                    break;
            }

            int count2 = 0;
            String query3 = "select count(*) from contenttb where content <> '' and " + cond;
            Cursor cc = db.rawQuery(query3, null);
            cc.moveToFirst();
            count2 = cc.getInt(0);
            cc.close();

            List<SeasonItem> list = new ArrayList<>();

            String query4 = "select * from contenttb where content <> '' and " + cond;
            Cursor c = db.rawQuery(query4, null);

            //App.Loger("Data Count in DB: " + count2);

            for (int i = 0; i < count2; i++) {

                c.moveToPosition(i);

                String arrengel1 = c.getString(1);
                String arrengel2 = c.getString(2);
                String arrengel3 = c.getString(3);
                String arrengel4 = c.getString(4);
                String titledb = c.getString(5);
                String contentdb = "";
                if (c.getString(6) != null) {
                    if (c.getString(6).trim().length() < 1) {
                        contentdb = "";
                    } else {
                        contentdb = c.getString(6);
                    }
                }

                String newarrenge = arrengel1;
                if (!arrengel2.equals("-")) {
                    newarrenge += "-" + arrengel2;
                }
                if (!arrengel3.equals("-")) {
                    newarrenge += "-" + arrengel3;
                }
                if (!arrengel4.equals("-")) {
                    newarrenge += "-" + arrengel4;
                }

                //App.Loger("Data added: " + newarrenge);
                SeasonItem si = new SeasonItem(c.getInt(0), newarrenge, titledb, contentdb);

                list.add(si);

            }


            c.close();
            return list;

        }


    }


    public SeasonItem getFullContent(int idz) {

        //String idstr = String.valueOf(idz);
        String queryz = "select * from contenttb where id = '" + idz + "'";
        Cursor c = db.rawQuery(queryz, null);
        c.moveToPosition(0);
        String arrengel1 = c.getString(1);
        String arrengel2 = c.getString(2);
        String arrengel3 = c.getString(3);
        String arrengel4 = c.getString(4);
        String titledb = c.getString(5);
        String contentdb = "";
        if (c.getString(6) != null) {
            if (c.getString(6).trim().length() < 1) {
                contentdb = "";
            } else {
                contentdb = c.getString(6);
            }
        }

        String newarrenge = arrengel1;
        if (!arrengel2.equals("-")) {
            newarrenge += "-" + arrengel2;
        }
        if (!arrengel3.equals("-")) {
            newarrenge += "-" + arrengel3;
        }
        if (!arrengel4.equals("-")) {
            newarrenge += "-" + arrengel4;
        }

        SeasonItem seasonItem = new SeasonItem(c.getInt(0), newarrenge, titledb, contentdb);
        c.close();

        return seasonItem;
    }


    public void addToFav(int id) {

        String tab = "contenttb";
        ContentValues row = new ContentValues();
        row.put("fav", 1);
        db.update(tab, row, "id=" + id, null);
    }

    public void removeFav(int id) {

        String tab = "contenttb";
        ContentValues row = new ContentValues();
        row.put("fav", 0);
        db.update(tab, row, "id=" + id, null);
    }

    public List<SeasonItem> getFavs() {

        int count = 0;
        String query = "select count(*) from contenttb where fav='1'";
        Cursor cc = db.rawQuery(query, null);
        cc.moveToFirst();
        count = cc.getInt(0);
        cc.close();

        if(count==0){
            return null;
        }

        List<SeasonItem> list = new ArrayList<>();

        String query2 = "select * from contenttb where fav='1'";
        Cursor c = db.rawQuery(query2, null);


        for (int i = 0; i < count; i++) {

            c.moveToPosition(i);


            String arrengel1 = c.getString(1);
            String arrengel2 = c.getString(2);
            String arrengel3 = c.getString(3);
            String arrengel4 = c.getString(4);
            String newarrenge = arrengel1;
            if (!arrengel2.equals("-")) {
                newarrenge += "-" + arrengel2;
            }
            if (!arrengel3.equals("-")) {
                newarrenge += "-" + arrengel3;
            }
            if (!arrengel4.equals("-")) {
                newarrenge += "-" + arrengel4;
            }


            String titledb = c.getString(5);
            String contentdb = "";
            if (c.getString(6) != null) {
                if (c.getString(6).trim().length() < 1) {
                    contentdb = "";
                } else {
                    contentdb = c.getString(6);
                }
            }

            //App.Loger("Data added: " + c.getString(1));
            SeasonItem si = new SeasonItem(c.getInt(0),newarrenge, titledb, contentdb);


            list.add(si);

        }


        c.close();
        return list;

    }

    public boolean isFav(int id){

        int fav = 0;
        String query = "select * from contenttb where id='"+id+"'";
        Cursor cc = db.rawQuery(query, null);
        cc.moveToFirst();
        fav = cc.getInt(7);
        cc.close();

        return fav==1;

    }

    public List<SeasonItem> SearchTitles(String text) {

        String query;
        String parts[] = text.split(" ");
        if(parts.length==1){
            query = "select * from contenttb where title like '%"+text+"%'";
        }else{

            String tosearch = "";
            for (int i = 0 ; i<parts.length ; i++){
                if(i==0){
                    tosearch += "%"+parts[i]+"%";
                }else {
                    tosearch += parts[i]+"%";
                }
            }

            //App.Loger("tosearch: " + tosearch);
            query = "select * from contenttb where title like '"+tosearch+"'";
        }

        List<SeasonItem> list = new ArrayList<>();

        Cursor c = db.rawQuery(query, null);

        if(c.getCount()==0){
            return null;
        }

        for (int i = 0; i < c.getCount() ; i++) {

            c.moveToPosition(i);

            String arrengel1 = c.getString(1);
            String arrengel2 = c.getString(2);
            String arrengel3 = c.getString(3);
            String arrengel4 = c.getString(4);
            String newarrenge = arrengel1;
            if (!arrengel2.equals("-")) {
                newarrenge += "-" + arrengel2;
            }
            if (!arrengel3.equals("-")) {
                newarrenge += "-" + arrengel3;
            }
            if (!arrengel4.equals("-")) {
                newarrenge += "-" + arrengel4;
            }

            String titledb = c.getString(5);
            String contentdb = "";
            if (c.getString(6) != null) {
                if (c.getString(6).trim().length() < 1) {
                    contentdb = "";
                } else {
                    contentdb = c.getString(6);
                }
            }

            SeasonItem si = new SeasonItem(c.getInt(0),newarrenge, titledb, contentdb);


            list.add(si);

        }


        c.close();
        return list;

    }

    public List<SeasonItem> SearchContent(String text) {

        String query;
        String parts[] = text.split(" ");
        if(parts.length==1){
            query = "select * from contenttb where content like '%"+text+"%' and content not like 'code==%'";
        }else{

            List<String> newparts = new ArrayList<>();

            for (int j = 0; j < parts.length; j++) {
                if (parts[j].length() > 1) { //search of single char take too long so we dont check it
                    newparts.add(parts[j]);
                }
            }

            String tosearch = "";
            for (int i = 0 ; i<newparts.size() ; i++){
                if(i==0){
                    tosearch += "%"+newparts.get(i)+"%";
                }else {
                    tosearch += newparts.get(i)+"%";
                }
            }

            App.Loger("tosearch: " + tosearch);
            query = "select * from contenttb where content like '"+tosearch+"' and content not like 'code==%'";
        }

        //query = "select * from contenttb where content <> ''";

        List<SeasonItem> list = new ArrayList<>();

        Cursor c = db.rawQuery(query, null);

        if(c.getCount()==0){
            return list;
        }

        for (int i = 0; i < c.getCount() ; i++) {

            c.moveToPosition(i);

            String arrengel1 = c.getString(1);
            String arrengel2 = c.getString(2);
            String arrengel3 = c.getString(3);
            String arrengel4 = c.getString(4);
            String newarrenge = arrengel1;
            if (!arrengel2.equals("-")) {
                newarrenge += "-" + arrengel2;
            }
            if (!arrengel3.equals("-")) {
                newarrenge += "-" + arrengel3;
            }
            if (!arrengel4.equals("-")) {
                newarrenge += "-" + arrengel4;
            }

            String titledb = c.getString(5);
            String contentdb = "";
            if (c.getString(6) != null) {
                if (c.getString(6).trim().length() < 1) {
                    contentdb = "";
                } else {
                    contentdb = c.getString(6);
                }
            }

            SeasonItem si = new SeasonItem(c.getInt(0),newarrenge, titledb, contentdb);


            list.add(si);

        }


        c.close();
        return list;

    }

    public String getAboutUs (){
        //String idstr = String.valueOf(idz);
        String queryz = "select * from about";
        Cursor c = db.rawQuery(queryz, null);
        c.moveToPosition(0);
        String cdata = c.getString(1);
        c.close();

        return cdata;
    }

    public List<SeasonItem> Select_Crypted_Data_To_Search() {

        String query;

        query = "select * from contenttb where content <> '' and content like 'code==%'";

        List<SeasonItem> list = new ArrayList<>();

        Cursor c = db.rawQuery(query, null);

        if(c.getCount()==0){
            return null;
        }

        for (int i = 0; i < c.getCount() ; i++) {

            c.moveToPosition(i);

            String arrengel1 = c.getString(1);
            String arrengel2 = c.getString(2);
            String arrengel3 = c.getString(3);
            String arrengel4 = c.getString(4);
            String newarrenge = arrengel1;
            if (!arrengel2.equals("-")) {
                newarrenge += "-" + arrengel2;
            }
            if (!arrengel3.equals("-")) {
                newarrenge += "-" + arrengel3;
            }
            if (!arrengel4.equals("-")) {
                newarrenge += "-" + arrengel4;
            }

            String titledb = c.getString(5);
            String contentdb = "";
            if (c.getString(6) != null) {
                if (c.getString(6).trim().length() < 1) {
                    contentdb = "";
                } else {
                    contentdb = c.getString(6);
                }
            }

            SeasonItem si = new SeasonItem(c.getInt(0),newarrenge, titledb, contentdb);


            list.add(si);

        }


        c.close();
        return list;

    }


    public void close() {

        try {
            db.close();
        }catch (Exception e){
            App.DialogMaker(contexts,"خطا","ورژن دیتابیس را بالا ببرید! و یا برنامه را یکبار پاک کنید و مجدد نصب کنید. لطفا ویدیو های آموزشی را با دقت مشاهده کنید تا به مشکلات این چنینی برخورد نکنید");
        }

    }


}
