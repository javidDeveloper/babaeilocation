package com.saeed.book;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.saeed.book.Adapter.SearchAdapter;
import com.saeed.book.Adapter.SeasonsAdapter;
import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.DataBase;
import com.saeed.book.lib.TextViewPlus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Developed and More: ariansource .com
 */
public class Search_all extends AppCompatActivity {


    EditText inputSearch;
    ImageView done;
    int theme = 0;

    boolean nav;

    SearchAdapter adapter;
    RecyclerView recyclerView;
    private List<SeasonItem> data = new ArrayList<>();
    Context c;
    DataBase db;
    boolean intenthappen = false;

    LinearLayout left_l;
    TextViewPlus left_t;
    LinearLayout right_l;
    TextViewPlus right_t;
    TextViewPlus not_found;
    ProgressBar loading;

    boolean search_in_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Settings_Activity.loadNightMode(this)) {
            setContentView(R.layout.sreach_all);
            theme = 0;
        } else {
            App.StatusColorDark(this);
            setContentView(R.layout.sreach_all_dark);
            theme = 1;
        }
        intenthappen = false;
        Intent intentsss = getIntent();

        db = new DataBase(this);
        c = Search_all.this;

        search_in_content = false;

        done = (ImageView) findViewById(R.id.done);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        left_l = (LinearLayout) findViewById(R.id.left_l);
        left_t = (TextViewPlus) findViewById(R.id.left_t);
        right_l = (LinearLayout) findViewById(R.id.right_l);
        right_t = (TextViewPlus) findViewById(R.id.right_t);
        loading = (ProgressBar) findViewById(R.id.loading);
        not_found = (TextViewPlus) findViewById(R.id.not_found);
        inputSearch = (EditText) findViewById(R.id.inputSearch);



        if (!Settings_Activity.loadNightMode(c)) {
            left_l.setBackgroundResource(R.drawable.search_selector_left);
        } else {
            left_l.setBackgroundResource(R.drawable.search_selector_left_dark);
        }
        right_l.setBackgroundResource(R.drawable.search_selector_right_selected);
        right_t.setTextColor(ContextCompat.getColor(c, R.color.text_light));
        left_t.setTextColor(ContextCompat.getColor(c, R.color.text_middle));


        left_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                search_in_content = true;
                left_l.setBackgroundResource(R.drawable.search_selector_left_selected);
                if (!Settings_Activity.loadNightMode(c)) {
                    right_l.setBackgroundResource(R.drawable.search_selector_right);
                } else {
                    right_l.setBackgroundResource(R.drawable.search_selector_right_dark);
                }
                right_t.setTextColor(ContextCompat.getColor(c, R.color.text_middle));
                left_t.setTextColor(ContextCompat.getColor(c, R.color.text_light));


                StartSearching(false);

            }
        });

        right_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                search_in_content = false;
                if (!Settings_Activity.loadNightMode(c)) {
                    left_l.setBackgroundResource(R.drawable.search_selector_left);
                } else {
                    left_l.setBackgroundResource(R.drawable.search_selector_left_dark);
                }
                right_l.setBackgroundResource(R.drawable.search_selector_right_selected);
                right_t.setTextColor(ContextCompat.getColor(c, R.color.text_light));
                left_t.setTextColor(ContextCompat.getColor(c, R.color.text_middle));

                StartSearching(false);

            }
        });

        not_found.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);


        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    StartSearching(true);

                    return true;
                }
                return false;
            }
        });


        inputSearch.setTypeface(App.vazir);


//
//        inputSearch.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//
//
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//                                          int arg3) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//                // TODO Auto-generated method stub
//            }
//        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StartSearching(true);

            }
        });


    }


    public void SearchInContents(final String text_to_search) {


        adapter = new SearchAdapter(data, c, text_to_search, false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AddClickListener(true,text_to_search);

        loading.setVisibility(View.VISIBLE);
        not_found.setVisibility(View.GONE);

        data.clear();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                LoadAllDataAndFilter(text_to_search);

            }
        }, 10);


    }

    public void SearchInTitles(String text_to_search) {

        adapter = new SearchAdapter(data, c, text_to_search, true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        AddClickListener(false,text_to_search);

        loading.setVisibility(View.VISIBLE);
        not_found.setVisibility(View.GONE);
        data.clear();

        List<SeasonItem> items = new ArrayList<>();
        items = db.SearchTitles(text_to_search);

        if (items == null) {
            loading.setVisibility(View.GONE);
            not_found.setVisibility(View.VISIBLE);
        } else {
            data.addAll(items);
            adapter.notifyDataSetChanged();
            loading.setVisibility(View.GONE);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        App.HandlerPauseBackgroundMusic(intenthappen,this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int theme2;
        if(!Settings_Activity.loadNightMode(this)){
            theme2 = 0;
        }else{
            theme2 = 1;
        }

        if(theme != theme2){
            Search_all.this.recreate();
        }
        App.HandlerResumeBackgroundMusic(this);

        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            //e.printStackTrace();
        }

    }


    private void AddClickListener(final boolean content,final String text) {
        adapter.setClickCallBack(
                new SearchAdapter.ItemClickCallBack() {
                    @Override
                    public void onItemClick(int pos) {

                        if (data.get(pos).getSeasonContent().equals("")) {

                            intenthappen = true;
                            Intent i = new Intent(getApplicationContext(), SeasonMenu.class);
                            String arrenge = data.get(pos).getSeasonArrenge();
                            App.Loger("ID to send" + arrenge);
                            i.putExtra("arrenge", arrenge);
                            i.putExtra("arrenge_name", data.get(pos).getSeasonTitle());
                            startActivity(i);

                        } else {
                            //App.ToastMaker(c, "Data Exist");

                            intenthappen = true;
                            Intent i = new Intent(getApplicationContext(), ShowContent.class);
                            int id = data.get(pos).getSeasonID();
                            App.Loger("ID to send" + id);
                            i.putExtra("id", id);
                            i.putExtra("fav_or_search", true);
                            if(content){
                                i.putExtra("search_in_text", text);
                            }
                            startActivity(i);

                        }


                    }
                }
        );
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }


    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }

    }


    public void StartSearching(boolean show_alerts) {

        if (show_alerts) hideKeyboard();

        if (inputSearch.getText().toString().trim().matches("")) {
            if (show_alerts) App.ToastMaker(c, "لطفا متن جستجو را وارد کنید");
        } else {

            if (inputSearch.getText().toString().trim().length() < 3) {
                if (show_alerts) App.ToastMaker(c, "ورود حداقل 3 حرف برای جستجو لازم است");
            } else {

                String tosearch = inputSearch.getText().toString();

                boolean all_single_char = true;
                String[] parts = tosearch.split(" ");
                for (int i=0 ; i < parts.length ; i++){
                    if(parts[i].length()>1){
                        all_single_char = false;
                    }
                }

                if(all_single_char){

                    if (show_alerts) App.ToastMaker(c, "لطفا حداقل یه کلمه بامعنی وارد کنید");

                }else{
                    if (search_in_content) {
                        SearchInContents(tosearch);
                    } else {
                        SearchInTitles(tosearch);
                    }
                }



            }
        }

    }


    private void LoadAllDataAndFilter(String text_to_search) {

        List<SeasonItem> items = new ArrayList<>();
        List<SeasonItem> filteritems = new ArrayList<>();
        List<SeasonItem> items_normalsearch = new ArrayList<>();
        items = db.Select_Crypted_Data_To_Search(); // actualy it take all datas (for decrypt its needed)
        items_normalsearch = db.SearchContent(text_to_search);

        if(items != null){

            for (int i = 0; i < items.size(); i++) { //search encrypted_data

                String content;
                content = ShowContent.decryptIt(items.get(i).getSeasonContent(),c);
                items.get(i).setSeasonContent(content);


                if (content.contains(text_to_search)) {

                    App.Loger("text_to_search exactly matched: " + items.get(i).getSeasonTitle());
                    filteritems.add(items.get(i));

                } else {

                    if (text_to_search.contains(" ")) {

                        boolean containes_all = true;

                        String parts[] = text_to_search.split(" ");

                        List<String> newparts = new ArrayList<>();

                        for (int j = 0; j < parts.length; j++) {
                            if (parts[j].length() > 1) { //search of single char take too long so we dont check it
                                newparts.add(parts[j]);
                            }
                        }

                        for (int j = 0; j < newparts.size(); j++) {

                            if (!content.contains(newparts.get(j))) {
                                containes_all = false;
                                break;
                            }

                        }

                        if (containes_all) {
                            App.Loger("text_to_search splited and finded: " + items.get(i).getSeasonTitle());
                            filteritems.add(items.get(i));
                        }

                    }

                }

            }

        }


        if (items_normalsearch.size() > 0) {
            filteritems.addAll(items_normalsearch);
        }


        if (text_to_search.contains(" ") && filteritems.size() > 0) {
            filteritems = SwapExactMatchs(filteritems, text_to_search);
        }

        if (filteritems.size() == 0) {
            loading.setVisibility(View.GONE);
            not_found.setVisibility(View.VISIBLE);
        } else {
            data.addAll(filteritems);
            adapter.notifyDataSetChanged();
            loading.setVisibility(View.GONE);
        }

    }


    private List<SeasonItem> SwapExactMatchs(List<SeasonItem> items, String text_to_search) {

        int pos = 0;

        for (int i = 0; i < items.size(); i++) {

            if (items.get(i).getSeasonContent().contains(text_to_search)) {

                if (i != pos) {
                    Collections.swap(items, i, pos);
                }
                pos++;

            }

        }

        return items;

    }

}

