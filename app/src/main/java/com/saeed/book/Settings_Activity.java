package com.saeed.book;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.ToggleButton;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Settings_Activity extends AppCompatActivity {

    Typeface vazir;
    Typeface vazirb;
    Typeface shabnam;
    Typeface shabnamb;
    Typeface sahel;
    Typeface sahelb;

    TextView sample;
    TextView s2;
    TextView s4;
    int size = 0;
    int api;
    LinearLayout keepon;
    LinearLayout night;
    LinearLayout music;
    SwitchCompat nightmode;
    SwitchCompat screenon;
    SwitchCompat musict;
    ImageView back;


    public static boolean isitnight;
    boolean intenthappen = false;


    @SuppressWarnings("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        intenthappen = false;
        isitnight = loadNightMode(this);
        if(isitnight){
            App.StatusColorDark(this);
            setContentView(R.layout.settings_layout_dark);
        }else{
            setContentView(R.layout.settings_layout);
        }

        vazir = Typeface.createFromAsset(getAssets(), "fonts/vazir.ttf");
        vazirb = Typeface.createFromAsset(getAssets(), "fonts/vazirb.ttf");
        shabnam = Typeface.createFromAsset(getAssets(), "fonts/shabnam.ttf");
        shabnamb = Typeface.createFromAsset(getAssets(), "fonts/shabnamb.ttf");
        sahel = Typeface.createFromAsset(getAssets(), "fonts/sahel.ttf");
        sahelb = Typeface.createFromAsset(getAssets(), "fonts/sahelb.ttf");

        s2 = (TextView) findViewById(R.id.s2);
        s4 = (TextView) findViewById(R.id.size);
        sample = (TextView) findViewById(R.id.sample);

        keepon = (LinearLayout) findViewById(R.id.keepon);
        night = (LinearLayout) findViewById(R.id.night);
        music = (LinearLayout) findViewById(R.id.music);

        nightmode = (SwitchCompat) findViewById(R.id.nightmode);
        screenon = (SwitchCompat) findViewById(R.id.screenon);
        musict = (SwitchCompat) findViewById(R.id.musict);
        back = (ImageView) findViewById(R.id.back_arrow);

        api = Build.VERSION.SDK_INT;

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(App.Music){
            music.setVisibility(View.VISIBLE);
        }else {
            music.setVisibility(View.GONE);
        }

        if(App.Change_Day_Night_Mode_Settings){
            night.setVisibility(View.VISIBLE);
        }else {
            night.setVisibility(View.GONE);
        }

        if(loadKeepOn(Settings_Activity.this)){
            screenon.setChecked(true);
        }else{
            screenon.setChecked(false);
        }
        if(loadNightMode(Settings_Activity.this)){
            nightmode.setChecked(true);
        }else{
            nightmode.setChecked(false);
        }
        if(loadMusic(Settings_Activity.this)){
            musict.setChecked(true);
        }else{
            musict.setChecked(false);
        }

        keepon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loadKeepOn(Settings_Activity.this)){
                    screenon.setChecked(false);
                    saveKeepOn(false,Settings_Activity.this);
                }else{
                    screenon.setChecked(true);
                    saveKeepOn(true,Settings_Activity.this);
                }
            }
        });

        screenon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b){
                    saveKeepOn(false,Settings_Activity.this);
                }else{
                    saveKeepOn(true,Settings_Activity.this);
                }
            }
        });




        night.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loadNightMode(Settings_Activity.this)){
                    nightmode.setChecked(false);
                    saveNightMode(false,Settings_Activity.this);
                    restart();

                }else{
                    nightmode.setChecked(true);
                    saveNightMode(true,Settings_Activity.this);
                    restart();
                }
            }
        });

        nightmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b){
                    saveNightMode(false,Settings_Activity.this);
                    restart();
                }else{
                    saveNightMode(true,Settings_Activity.this);
                    restart();
                }
            }
        });

        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loadMusic(Settings_Activity.this)){
                    musict.setChecked(false);
                    saveMusic(false,Settings_Activity.this);
                    stopService(new Intent(Settings_Activity.this,BackgroundSoundService.class));

                }else{
                    musict.setChecked(true);
                    saveMusic(true,Settings_Activity.this);
                    startService(new Intent(Settings_Activity.this,BackgroundSoundService.class));
                }
            }
        });

        musict.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b){
                    saveMusic(false,Settings_Activity.this);
                    stopService(new Intent(Settings_Activity.this,BackgroundSoundService.class));
                }else{
                    saveMusic(true,Settings_Activity.this);
                    startService(new Intent(Settings_Activity.this,BackgroundSoundService.class));
                }
            }
        });


        NiceSpinner s = (NiceSpinner) findViewById(R.id.spinner_font);
        List<String> dataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.font_selector)));
        s.attachDataSource(dataset);


        size = loadFontSize(Settings_Activity.this);
        sample.setTextSize(size);
        s4.setText("سایز فعلی: " + size);

        switch (loadFontType(Settings_Activity.this)) {
            case 0:
                sample.setTypeface(vazir);
                s2.setText("فونت فعلی: " + getResources().getString(R.string.font1));
                break;
            case 1:
                sample.setTypeface(shabnam);
                s2.setText("فونت فعلی: " + getResources().getString(R.string.font2));
                break;
            case 2:
                sample.setTypeface(sahel);
                s2.setText("فونت فعلی: " + getResources().getString(R.string.font3));
                break;

        }

        if(!Settings_Activity.loadNightMode(this)){
            s.setTextColor(Color.parseColor("#202020"));
        }else{
            s.setTextColor(Color.parseColor("#ffffff"));
        }


        s.setSelectedIndex(loadFontType(Settings_Activity.this));

        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                saveFontType(position,Settings_Activity.this);
                switch (position){
                    case 0: sample.setTypeface(vazir); s2.setText("فونت فعلی: " + getResources().getString(R.string.font1)); break;
                    case 1: sample.setTypeface(shabnam); s2.setText("فونت فعلی: " + getResources().getString(R.string.font2)); break;
                    case 2: sample.setTypeface(sahel); s2.setText("فونت فعلی: " + getResources().getString(R.string.font3)); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        DiscreteSeekBar seek = (DiscreteSeekBar) findViewById(R.id.seekBar);

        seek.setMin(App.Min_Font_Size);
        seek.setMax(App.Max_Font_Size);

        seek.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                size = value;
                saveFontSize(size,Settings_Activity.this);
                sample.setTextSize(size);
                s4.setText("سایز فعلی: " + size);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });


        size = loadFontSize(Settings_Activity.this);
        sample.setTextSize(size);
        s4.setText("سایز فعلی: " + size);
        seek.setProgress(size);

        LinearLayout mail = (LinearLayout) findViewById(R.id.email);

        if(App.DeveloperEmail.equals("")){
            mail.setVisibility(View.GONE);
        }

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{App.DeveloperEmail});
                i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                try {
                    startActivity(Intent.createChooser(i, "Send Email..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(Settings_Activity.this, "برنامه جیمیل را نصب ندارید؟!", Toast.LENGTH_SHORT).show();
                }
            }
        });





    }

    @Override
    public void onBackPressed() {
        intenthappen =true;
        finish();
    }



    public static void saveFontType(int value,Context c) {
        SharedPreferences sp = c.getSharedPreferences("font", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("font", value);
        editor.commit();
    }
    public static int loadFontType(Context c) {
        SharedPreferences sp = c.getSharedPreferences("font", Activity.MODE_PRIVATE);
        int check = sp.getInt("font", 100);
        if(check == 100){
            Log.e("s","test");
            switch (App.Defualt_Font){
                case 1:
                    return sp.getInt("font", 0);
                case 2:
                    return sp.getInt("font", 1);
                case 3:
                    return sp.getInt("font", 2);
                case 4:
                    return sp.getInt("font", 3);
                case 5:
                    return sp.getInt("font", 4);
                case 6:
                    return sp.getInt("font", 5);
                default:
                    return sp.getInt("font", 0);

            }
        }else{
            return sp.getInt("font", 0);
        }
    }


    public static void saveFontSize(int value,Context c) {
        SharedPreferences sp = c.getSharedPreferences("size", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("size", value);
        editor.commit();
    }
    public static int loadFontSize(Context c) {
        SharedPreferences sp = c.getSharedPreferences("size", Activity.MODE_PRIVATE);
        return sp.getInt("size",App.Defualt_Font_Size);
    }


    public static void saveKeepOn(boolean value,Context c) {
        SharedPreferences sp = c.getSharedPreferences("keep", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("keep", value);
        editor.commit();
        //Log.e("s","screen on="+value);
    }
    public static boolean loadKeepOn(Context c) {
        SharedPreferences sp = c.getSharedPreferences("keep", Activity.MODE_PRIVATE);
        return sp.getBoolean("keep", true);
    }


    public static void saveNightMode(boolean value,Context c) {
        SharedPreferences sp = c.getSharedPreferences("nm", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("nm", value);
        editor.commit();
        //Log.e("s","nightmode="+value);
    }
    public static boolean loadNightMode(Context c) {
        SharedPreferences sp = c.getSharedPreferences("nm", Activity.MODE_PRIVATE);
        return sp.getBoolean("nm", false);
    }


    public static void saveMusic(boolean value,Context c) {
        SharedPreferences sp = c.getSharedPreferences("mus", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("mus", value);
        editor.commit();
    }
    public static boolean loadMusic(Context c) {
        SharedPreferences sp = c.getSharedPreferences("mus", Activity.MODE_PRIVATE);
        return sp.getBoolean("mus", App.FirstRunMusic);
    }




    public void restart(){

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                intenthappen = true;
                Settings_Activity.this.recreate();
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            }
        }, 200);

    }



    @Override
    protected void onPause() {
        super.onPause();
        App.HandlerPauseBackgroundMusic(intenthappen,this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.HandlerResumeBackgroundMusic(this);
    }
}




//BookSource 4 by ariansource.com
// in source faghat az tarigh hamin site forokhte misshavad
// dar sorati ke site digari in source ra beforosh miresand loffan etela dahid
//   batashakor. saeed.dc2011@gmail.com