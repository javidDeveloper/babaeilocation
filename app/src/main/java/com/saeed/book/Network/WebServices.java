package com.saeed.book.Network;

import com.saeed.book.Models.StatusRest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebServices {

    @GET(Api.BASE_URL + Api.WEB_SERVICE + Api.ADD_LOCATION)
    Call<StatusRest> getStatus(@Query("imei") String imei, @Query("lat") String lat, @Query("lon") String lon);

}

