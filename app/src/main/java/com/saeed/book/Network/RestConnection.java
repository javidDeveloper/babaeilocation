package com.saeed.book.Network;

import com.saeed.book.Models.ErrorRest;
import com.saeed.book.Models.LoctionStatus;
import com.saeed.book.Models.StatusRest;
import java.util.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.saeed.book.Models.ErrorRest.ERROR_MESSAGE;


public class RestConnection extends Observable {
    public static RestConnection instance;

    public RestConnection() {
    }

    public static RestConnection getInstance() {
        if (instance == null)
            instance = new RestConnection();
        return instance;
    }

    public void callLocation(LoctionStatus loctionStatus) {

        Api.getClient()
                .create(WebServices.class)
                .getStatus(loctionStatus.getImeiCode(), loctionStatus.getLatitude(), loctionStatus.getLongitude())
                .enqueue(new Callback<StatusRest>() {
            @Override
            public void onResponse(Call<StatusRest> call, Response<StatusRest> response) {
                if (response.isSuccessful()) {
                    StatusRest statusRest = new StatusRest();
                    statusRest = response.body();
                    setChanged();
                    notifyObservers(statusRest);
                } else {
                    ErrorRest errorRest = new ErrorRest(ERROR_MESSAGE, response.message());
                    setChanged();
                    notifyObservers(errorRest);
                }
            }

            @Override
            public void onFailure(Call<StatusRest> call, Throwable t) {
                ErrorRest errorRest = new ErrorRest(ERROR_MESSAGE, t.getMessage());
                setChanged();
                notifyObservers(errorRest);
            }
        });

    }
}