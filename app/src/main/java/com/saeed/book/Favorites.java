package com.saeed.book;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.saeed.book.Adapter.SeasonsAdapter;
import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.DataBase;
import com.saeed.book.lib.RtlGridLayoutManager;
import com.saeed.book.lib.TextViewPlus;


public class Favorites extends AppCompatActivity {

    public boolean Empty = true;
    ImageView back;
    ImageView emptytext;
    List<Integer> images;
    boolean intenthappen = false;
    int theme = 0;

    DataBase db;

    Context c;

    SeasonsAdapter adapter;
    RecyclerView recyclerView;
    RecyclerView recyclerView_grid;
    private List<SeasonItem> all_datas = new ArrayList<>();

    @SuppressWarnings("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intenthappen = false;
        if(!Settings_Activity.loadNightMode(this)){
            setContentView(R.layout.favorites);
            theme = 0;
        }else{
            App.StatusColorDark(this);
            setContentView(R.layout.favorites_dark);
            theme = 1;
        }

        c = Favorites.this;

        db = new DataBase(this);

        back = (ImageView) findViewById(R.id.back_arrow);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        recyclerView_grid = (RecyclerView) findViewById(R.id.recyclerView_grid);
        emptytext = (ImageView) findViewById(R.id.empty);

        if(db.getFavs() == null){
            emptytext.setVisibility(View.VISIBLE);
        }else {
            all_datas = db.getFavs();

            adapter = new SeasonsAdapter(all_datas , c , App.EnableGrid);

            if(App.EnableGrid){
                int numberOfColumns = App.calculateNoOfColumns(c);
                RecyclerView.LayoutManager grid_manager = new RtlGridLayoutManager(c, numberOfColumns);
                recyclerView_grid.setLayoutManager(grid_manager);
                recyclerView.setVisibility(View.GONE);
                recyclerView_grid.setVisibility(View.VISIBLE);
                recyclerView_grid.setItemAnimator(new DefaultItemAnimator());
                recyclerView_grid.setAdapter(adapter);
            }else{
                RecyclerView.LayoutManager linear_manager = new LinearLayoutManager(c);
                recyclerView.setLayoutManager(linear_manager);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView_grid.setVisibility(View.GONE);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }

            adapter.setClickCallBack(
                    new SeasonsAdapter.ItemClickCallBack() {
                        @Override
                        public void onItemClick(int pos) {

                            if (!all_datas.get(pos).getSeasonContent().equals("")) {

                                intenthappen = true;
                                Intent i = new Intent(getApplicationContext(), ShowContent.class);
                                int id = all_datas.get(pos).getSeasonID();
                                App.Loger("ID to send" + id);
                                i.putExtra("id", id);
                                i.putExtra("fav_or_search", true);
                                startActivity(i);


                            }

                        }
                    }
            );

        }






        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        images = new ArrayList<Integer>();






    }


    @Override
    public void onBackPressed() {
        intenthappen =true;
        super.onBackPressed();
    }





    @Override
    protected void onPause() {
        super.onPause();
        App.HandlerPauseBackgroundMusic(intenthappen,this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int theme2;
        if(!Settings_Activity.loadNightMode(this)){
            theme2 = 0;
        }else{
            theme2 = 1;
        }

        if(theme != theme2){
            Favorites.this.recreate();
        }

        App.HandlerResumeBackgroundMusic(c);

        if(db.getFavs() == null){
            recyclerView.setVisibility(View.GONE);
            emptytext.setVisibility(View.VISIBLE);
        }else {
            all_datas.clear();
            all_datas.addAll(db.getFavs());
            adapter.notifyDataSetChanged();
        }

    }

}









//BookSource 4 by ariansource.com
// in source faghat az tarigh hamin site forokhte misshavad
// dar sorati ke site digari in source ra beforosh miresand loffan etela dahid
//   batashakor. saeed.dc2011@gmail.com