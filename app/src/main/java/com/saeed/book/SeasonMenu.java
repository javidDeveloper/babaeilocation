package com.saeed.book;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.saeed.book.Adapter.SeasonsAdapter;
import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.DataBase;
import com.saeed.book.lib.RtlGridLayoutManager;
import com.saeed.book.lib.TextViewPlus;


import java.util.ArrayList;
import java.util.List;


public class SeasonMenu extends AppCompatActivity {

    ImageView back;
    ImageView favs;
    ImageView search;
    List<Integer> images;
    boolean intenthappen = false;
    DataBase db;
    Context c;
    SeasonsAdapter adapter;
    RecyclerView recyclerView;
    RecyclerView recyclerView_grid;
    private List<SeasonItem> levels = new ArrayList<>();
    TextViewPlus ScreenTitle;
    Animation slidein;
    Animation slideback;
    ProgressBar loading;
    FrameLayout mainframelayout;
    int theme = 0;


    List<SeasonItem> all_datas = new ArrayList<>();
    String arrenge;

    @SuppressWarnings("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (!Settings_Activity.loadNightMode(this)) {
            setContentView(R.layout.season_menu);
            theme = 0;
        } else {
            App.StatusColorDark(this);
            setContentView(R.layout.season_menu_dark);
            theme = 1;
        }
        c = SeasonMenu.this;

        String arrenge_name;
        Intent intent = getIntent();
        if(intent.hasExtra("arrenge")) {
            arrenge = intent.getStringExtra("arrenge");
            arrenge_name = intent.getStringExtra("arrenge_name");
        }else {
            arrenge = "";
            arrenge_name = "";
        }

        images = new ArrayList<Integer>();
        db = new DataBase(c);

        //SQLiteDatabase.loadLibs(c);
        InitView();





        if(arrenge.equals("")){

            all_datas = db.getRows("0");
            SeasonItem i0 = new SeasonItem(1,"0",getResources().getString(R.string.seasons),"");
            levels.add(i0);

        }else {

            ScreenTitle.setText(arrenge_name);
            all_datas = db.getRows(arrenge);
            SeasonItem i0 = new SeasonItem(1,arrenge,arrenge_name,"");
            levels.add(i0);

        }

        if(App.JumpToSeasons){
            favs.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
        }else{
            favs.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            back.setVisibility(View.VISIBLE);
        }

        favs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(c,Favorites.class));
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(c,Search_all.class));
            }
        });

        loading.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BackHandler();
            }
        });

        adapter = new SeasonsAdapter(all_datas , c , App.EnableGrid);


        if(App.EnableGrid){
            int numberOfColumns = App.calculateNoOfColumns(c);
            RecyclerView.LayoutManager grid_manager = new RtlGridLayoutManager(c, numberOfColumns);
            recyclerView_grid.setLayoutManager(grid_manager);
            recyclerView.setVisibility(View.GONE);
            recyclerView_grid.setVisibility(View.VISIBLE);
            recyclerView_grid.setItemAnimator(new DefaultItemAnimator());
            recyclerView_grid.setAdapter(adapter);
        }else{
            RecyclerView.LayoutManager linear_manager = new LinearLayoutManager(c);
            recyclerView.setLayoutManager(linear_manager);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView_grid.setVisibility(View.GONE);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }



        slidein = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        slideback = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);

        adapter.setClickCallBack(
                new SeasonsAdapter.ItemClickCallBack() {
                    @Override
                    public void onItemClick(int pos) {

                        if (all_datas.get(pos).getSeasonContent().equals("")) {

                            if(App.JumpToSeasons){
                                back.setVisibility(View.VISIBLE);
                                favs.setVisibility(View.GONE);
                                search.setVisibility(View.GONE);
                            }

                            ScreenTitle.setText(all_datas.get(pos).getSeasonTitle());

                            mainframelayout.setVisibility(View.GONE);
                            String arrenge = all_datas.get(pos).getSeasonArrenge();

                            //App.Loger("Curr = " + arrenge);
                            SeasonItem seasonItem = new SeasonItem(1,arrenge,all_datas.get(pos).getSeasonTitle(),"");
                            levels.add(seasonItem);


                            all_datas.clear();
                            all_datas.addAll(db.getRows(arrenge));
                            adapter.notifyDataSetChanged();

                            mainframelayout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.VISIBLE);
                            mainframelayout.startAnimation(slidein);

                            loading.setVisibility(View.GONE);


                        } else {
                            //App.ToastMaker(c, "Data Exist");

                            intenthappen = true;
                            Intent i = new Intent(getApplicationContext(), ShowContent.class);
                            int id = all_datas.get(pos).getSeasonID();
                            App.Loger("ID to send" + id);
                            i.putExtra("id", id);
                            startActivity(i);

                        }


                    }
                }
        );


//        list = new ArrayList<String>();
//
//        intenthappen = false;
//
//        adapternav = new SaeedAdapter(SeasonMenu.this);
//        setListAdapter(adapternav);
//
//        lv = getListView();
//        if(App.Divider){
//            ColorDrawable sage;
//            if(Settings_Activity.loadNightMode(this)){
//                sage = new ColorDrawable(this.getResources().getColor(R.color.night_back_lighter));
//            }else{
//                sage = new ColorDrawable(this.getResources().getColor(R.color.divider));
//            }
//
//            lv.setDivider(sage);
//            lv.setDividerHeight(2);
//        }
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, final View view,
//                                    int position, long id) {
////                intenthappen =true;
////                Intent i = new Intent(getApplicationContext(), ContentMenu.class);
////                int s = position + 1;
////                i.putExtra("s", s);
////
////                startActivity(i);
////                finish();
//            }
//
//        });


    }


    public static boolean isFirstTimeinmenu(Context c) {
        SharedPreferences sp = c.getSharedPreferences("f", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        boolean runedbefore = sp.getBoolean("f", false);
        if (!runedbefore) {
            editor.putBoolean("f", true);
            editor.commit();
        }
        return !runedbefore;
    }


    @Override
    public void onBackPressed() {
        BackHandler();
    }

    private void BackHandler() {
        if (levels.size() == 1) {


            if(App.JumpToSeasons){
                ExitHandler();
            }else{
                intenthappen = true;
                finish();
            }

        } else {


            mainframelayout.setVisibility(View.GONE);
            String arrenge = levels.get(levels.size() - 2).getSeasonArrenge();
            ScreenTitle.setText(levels.get(levels.size() - 2).getSeasonTitle());

            App.Loger("Prev = " + arrenge);

            all_datas.clear();
            all_datas.addAll(db.getRows(arrenge));
            adapter.notifyDataSetChanged();

            mainframelayout.setVisibility(View.VISIBLE);
            loading.setVisibility(View.VISIBLE);
            mainframelayout.startAnimation(slideback);

            loading.setVisibility(View.GONE);

            levels.remove(levels.size() - 1);

            if(App.JumpToSeasons){
                if(levels.size()==1){
                    back.setVisibility(View.GONE);
                    favs.setVisibility(View.VISIBLE);
                    search.setVisibility(View.VISIBLE);
                }
            }

        }
    }


    boolean doubleBackToExitPressedOnce = false;
    private void ExitHandler(){
        if(App.DoubleTapToExit){

            if (doubleBackToExitPressedOnce) {
                if(Build.VERSION.SDK_INT>=16){
                    finishAffinity();
                    stopService(new Intent(SeasonMenu.this,BackgroundSoundService.class));
                }else{
                    finish();
                    stopService(new Intent(SeasonMenu.this,BackgroundSoundService.class));
                }
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.exittoast) , Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }else{

            doubleBackToExitPressedOnce = false;
            AlertDialog.Builder alert;

            if(!Settings_Activity.loadNightMode(this)){
                alert = new AlertDialog.Builder(this,5);
            }else{
                alert = new AlertDialog.Builder(this, 4);
            }

            alert.setMessage("تایید خروج؟");
            alert.setPositiveButton("بله", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(Build.VERSION.SDK_INT>=16){
                        finishAffinity();
                        stopService(new Intent(SeasonMenu.this,BackgroundSoundService.class));
                    }else{
                        finish();
                        stopService(new Intent(SeasonMenu.this,BackgroundSoundService.class));
                    }


                }

            });
            alert.setNegativeButton("خیر", null);
            alert.show();
        }
    }

    private void InitView() {
        back = (ImageView) findViewById(R.id.back_arrow);
        favs = (ImageView) findViewById(R.id.favs);
        search = (ImageView) findViewById(R.id.search);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        recyclerView_grid = (RecyclerView) findViewById(R.id.recyclerView_grid);
        ScreenTitle = (TextViewPlus) findViewById(R.id.screentitle);
        loading = (ProgressBar) findViewById(R.id.progressBar);
        mainframelayout = (FrameLayout) findViewById(R.id.mainframelayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.HandlerPauseBackgroundMusic(intenthappen,this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int theme2;
        if(!Settings_Activity.loadNightMode(this)){
            theme2 = 0;
        }else{
            theme2 = 1;
        }

        if(theme != theme2){
            SeasonMenu.this.recreate();
        }
        App.HandlerResumeBackgroundMusic(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }


}

// Developer : Saeed Arianmanesh

// Website : ariansource.com

//  A R I A N S O U R C E . C O M