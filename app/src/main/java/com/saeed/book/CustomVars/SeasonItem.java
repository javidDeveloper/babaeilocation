package com.saeed.book.CustomVars;

import java.io.Serializable;

public class SeasonItem implements Serializable {
    private int id;
    private String season_arrenge,season_title,season_content;

    public SeasonItem() {
    }

    public SeasonItem(int id,String season_arrenge, String season_title, String season_content) {

        this.id = id;
        this.season_arrenge = season_arrenge;
        this.season_title = season_title;
        this.season_content = season_content;
        //this.season_content_decrypted = season_content_decrypted;

    }

    public int getSeasonID() {
        return id;
    }
    public void setSeasonID(int idd) {
        this.id = idd;
    }


    public String getSeasonArrenge() {
        return season_arrenge;
    }
    public void setSeasonArrenge(String arrenge) {
        this.season_arrenge = arrenge;
    }


    public String getSeasonTitle() {
        return season_title;
    }
    public void setSeasonTitle(String title) {
        this.season_title = title;
    }


    public String getSeasonContent() {
        return season_content;
    }
    public void setSeasonContent(String content) {
        this.season_content = content;
    }

//    public String getSeasonContentDecrypted() {
//        return season_content_decrypted;
//    }
//    public void setSeasonContentDecrypted(String content) {
//        this.season_content_decrypted = content;
//    }

//    public String toJson() {
//        Gson gson = new Gson();
//        return gson.toJson(this);
//    }
//
//    public static Taxonomy fromJson(String json) {
//        Gson gson = new Gson();
//        return gson.fromJson(json, Taxonomy.class);
//    }

}