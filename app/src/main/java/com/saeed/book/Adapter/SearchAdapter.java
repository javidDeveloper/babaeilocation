package com.saeed.book.Adapter;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.R;
import com.saeed.book.Settings_Activity;
import com.saeed.book.lib.App;
import com.saeed.book.lib.TextViewPlus;
//import com.xeoh.android.texthighlighter.TextHighlighter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    public void setClickCallBack(ItemClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

    public interface ItemClickCallBack {
        void onItemClick(int pos);
    }


    private List<SeasonItem> data_list = null;
    private ItemClickCallBack clickCallBack;
    private Context c;
    private String searchedtext;
    private boolean in_titles;


    public SearchAdapter(List<SeasonItem> datas, Context c, String searchedtext, boolean in_titles) {
        this.data_list = datas;
        this.c = c;
        this.searchedtext = searchedtext;
        this.in_titles = in_titles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if(!Settings_Activity.loadNightMode(c)){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
            return new SearchAdapter.ViewHolder(view);
        }else{
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_dark, viewGroup, false);
            return new SearchAdapter.ViewHolder(view);
        }


    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.title.setText(data_list.get(position).getSeasonTitle());

        if (in_titles) {

            if (data_list.get(position).getSeasonTitle().contains(searchedtext)) {

//                TextHighlighter textHighlighter = new TextHighlighter();
//
//                if (!Settings_Activity.loadNightMode(c)) {
//                    textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//                } else {
//                    textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//                }
//                textHighlighter.setBold(false);
//                textHighlighter.setItalic(false);
//                textHighlighter.addTarget(viewHolder.title);
//                textHighlighter.highlight(searchedtext, TextHighlighter.BASE_MATCHER);

                highlightString(viewHolder.title,searchedtext);

            } else {

                //TextHighlighter textHighlighter = new TextHighlighter();
                String parts[] = searchedtext.split(" ");
                for (int i = 0; i < parts.length; i++) {

                    if(parts[i].length()>1){

//                        if (!Settings_Activity.loadNightMode(c)) {
//                            textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color));
//                        } else {
//                            textHighlighter.setBackgroundColor(c.getResources().getColor(R.color.search_highlight_color_dark));
//                        }
//                        textHighlighter.setBold(false);
//                        textHighlighter.setItalic(false);
//                        textHighlighter.addTarget(viewHolder.title);
//                        textHighlighter.highlight(parts[i], TextHighlighter.BASE_MATCHER);

                        highlightString(viewHolder.title,parts[i]);

                    }

                }
            }


        } else { // in content


            int line_counter = 0;

//            if(data_list.get(position).getSeasonContent().startsWith("code==")){
//                data_list.get(position).setSeasonContent(data_list.get(position).getSeasonContentDecrypted());
//            }

            if (data_list.get(position).getSeasonContent().contains(searchedtext)) {

                int i = MatchCounter(data_list.get(position).getSeasonContent(),searchedtext);

                String exact_search = i + " بار " + "\"" +searchedtext + "\"" + " یافت شد";

                viewHolder.search_result_exact.setVisibility(View.VISIBLE);
                viewHolder.search_result_exact.setText(exact_search);

                line_counter++;

            }

            if(searchedtext.contains(" ")){

                String toshow = "";
                String parts[] = searchedtext.split(" ");
                for (int i = 0; i < parts.length; i++) {

                    if(parts[i].length()>1){

                        int z = MatchCounter(data_list.get(position).getSeasonContent(),parts[i]);

                        if(toshow.equals("")){
                            toshow += z + " بار " + "\"" +parts[i] + "\"" + " یافت شد";
                            line_counter++;
                        }else {
                            if(line_counter < 3){
                                toshow += "\n";
                                toshow += z + " بار " + "\"" +parts[i] + "\"" + " یافت شد";
                                line_counter++;
                            }else{
                                toshow += "\n";
                                toshow += "...";
                                break;
                            }

                        }

                    }

                }

                if(!toshow.equals("")){
                    viewHolder.search_result.setVisibility(View.VISIBLE);
                    viewHolder.search_result.setText(toshow);
                }

            }





        }

        viewHolder.item_view.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (clickCallBack != null) {
                            clickCallBack.onItemClick(position);
                        }
                    }
                }
        );

        //App.Loger("resid  "+ data_list.get(position).getSeasonArrenge());

        int imageresid = App.GetTitleImage(c, data_list.get(position).getSeasonArrenge());
        if (imageresid != 0) {
            viewHolder.imtumb.setImageResource(imageresid);
        } else {
            viewHolder.imtumb.setImageResource(R.drawable.tag);
        }

        if (App.ShowArrowForSeasons) {

            if (data_list.get(position).getSeasonContent().equals("")) {
                viewHolder.has_more.setVisibility(View.VISIBLE);
            } else {
                viewHolder.has_more.setVisibility(View.GONE);
            }

        }


    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }


    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextViewPlus title;
        public TextViewPlus search_result_exact;
        public TextViewPlus search_result;
        public ImageView imtumb;
        public ImageView has_more;
        public LinearLayout item_view;

        public ViewHolder(View view) {
            super(view);

            title = (TextViewPlus) view.findViewById(R.id.Title_of_lv_items);
            search_result_exact = (TextViewPlus) view.findViewById(R.id.search_res_exact);
            search_result = (TextViewPlus) view.findViewById(R.id.search_res);
            imtumb = (ImageView) view.findViewById(R.id.titleimage);
            has_more = (ImageView) view.findViewById(R.id.has_more);
            item_view = (LinearLayout) view.findViewById(R.id.item_view);

        }
    }


    private int MatchCounter(String fulldata , String find){
        int z = 0;

        Pattern p = Pattern.compile(find,Pattern.LITERAL);
        Matcher m = p.matcher(fulldata);
        while (m.find()) {
            z++;
        }
        return z;
    }

    private void highlightString(TextView tv, String input) {

        SpannableString spannableString = new SpannableString(tv.getText());
//        BackgroundColorSpan[] backgroundSpans = spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
//
//        for (BackgroundColorSpan span: backgroundSpans) {
//            spannableString.removeSpan(span);
//        }

        int color;
        if (!Settings_Activity.loadNightMode(c)) {
            color = c.getResources().getColor(R.color.search_highlight_color);
        } else {
            color = c.getResources().getColor(R.color.search_highlight_color_dark);
        }

        int indexOfKeyword = spannableString.toString().indexOf(input);

        while (indexOfKeyword > -1) {
            //Create a background color span on the keyword
            spannableString.setSpan(new BackgroundColorSpan(color), indexOfKeyword, indexOfKeyword + input.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            //Get the next index of the keyword
            indexOfKeyword = spannableString.toString().indexOf(input, indexOfKeyword + input.length());
        }

        tv.setText(spannableString);

    }

}

