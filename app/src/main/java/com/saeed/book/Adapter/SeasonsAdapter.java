package com.saeed.book.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.saeed.book.CustomVars.SeasonItem;
import com.saeed.book.R;
import com.saeed.book.Settings_Activity;
import com.saeed.book.lib.App;
import com.saeed.book.lib.TextViewPlus;

import java.util.List;

public class SeasonsAdapter extends RecyclerView.Adapter<SeasonsAdapter.ViewHolder> {

    public void setClickCallBack(ItemClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

    public interface ItemClickCallBack{
        void onItemClick(int pos);
    }


    private List<SeasonItem> data_list = null;
    private ItemClickCallBack clickCallBack;
    private boolean grid;
    private Context c;


    public SeasonsAdapter(List<SeasonItem> datas, Context c , boolean grid) {
        this.data_list = datas;
        this.c = c;
        this.grid = grid;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(grid){

            if(!Settings_Activity.loadNightMode(c)){
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item, viewGroup, false);
                return new SeasonsAdapter.ViewHolder(view);
            }else{
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item_dark, viewGroup, false);
                return new SeasonsAdapter.ViewHolder(view);
            }
        }else{
            if(!Settings_Activity.loadNightMode(c)){
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
                return new SeasonsAdapter.ViewHolder(view);
            }else{
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_dark, viewGroup, false);
                return new SeasonsAdapter.ViewHolder(view);
            }

        }
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.title.setText(data_list.get(position).getSeasonTitle());

        viewHolder.item_view.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (clickCallBack != null) {
                            clickCallBack.onItemClick(position);
                        }
                    }
                }
        );

        //App.Loger("resid  "+ data_list.get(position).getSeasonArrenge());

        int imageresid = App.GetTitleImage(c,data_list.get(position).getSeasonArrenge());
        if(imageresid!=0){
            viewHolder.imtumb.setImageResource(imageresid);
        }else {
            viewHolder.imtumb.setImageResource(R.drawable.tag);
        }

        if(App.ShowArrowForSeasons){

            if(data_list.get(position).getSeasonContent().equals("")){
                viewHolder.has_more.setVisibility(View.VISIBLE);
            }else{
                viewHolder.has_more.setVisibility(View.GONE);
            }

        }



    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }


    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextViewPlus title;
        public ImageView imtumb;
        public ImageView has_more;
        public LinearLayout item_view;

        public ViewHolder(View view) {
            super(view);

            title = (TextViewPlus) view.findViewById(R.id.Title_of_lv_items);
            imtumb = (ImageView) view.findViewById(R.id.titleimage);
            has_more = (ImageView) view.findViewById(R.id.has_more);
            item_view = (LinearLayout) view.findViewById(R.id.item_view);

        }
    }


}

