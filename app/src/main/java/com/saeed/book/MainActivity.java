package com.saeed.book;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.saeed.book.Managers.AlarmsManager;
import com.saeed.book.Managers.LocationsManager;
import com.saeed.book.Models.ContextModel;
import com.saeed.book.lib.App;
import com.saeed.book.lib.BackgroundSoundService;
import com.saeed.book.lib.DataBase;

import io.michaelrocks.paranoid.Obfuscate;
import ir.basalam.rtlnavigationview.RtlNavigationView;

@Obfuscate
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    LinearLayout ratel;
    int theme = 0;
    ImageView header;
    DataBase db;
    Typeface vazirb;
    Typeface vazir;
    boolean intenthappen = false;
    RtlNavigationView rtlNavigationView;
    private boolean isDrawerOpened;
    private DrawerLayout drawer;
    private ImageView opener;
    private ImageView search;
    Button start;
    public static String Key = "saeedaaa";


    @SuppressWarnings("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContextModel.setCurrentActivity(this);
        statusCheck();
        if (App.JumpToSeasons) {
            startActivity(new Intent(MainActivity.this, SeasonMenu.class));
            finish();
        }


        if (!Settings_Activity.loadNightMode(this)) {
            setContentView(R.layout.main_menu);
            theme = 0;
        } else {
            App.StatusColorDark(this);
            setContentView(R.layout.main_menu_dark);
            theme = 1;
        }


        //App.ToastMaker(this,test);
        //new DataBaseAssetCopier(this);

        db = new DataBase(this);


        InitViews();

        if (App.Header) {
            if (!Settings_Activity.loadNightMode(this)) {
                header.setImageResource(R.mipmap.header);
            } else {
                header.setImageResource(R.mipmap.header_dark);
            }
        }

        if (!App.Rate_App) {
            ratel.setVisibility(View.GONE);
        }

        start.setTypeface(vazirb);

        ButtonsClickListener();

        DrawerBuilder();


    }//end of onCreate

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }
    private void buildAlertMessageNoGps() {
        //Enable GPS
        try {
            Toast.makeText(ContextModel.getContext(), "برای دریافت جدید ترین ها در اطراف شما نیاز به دسترسی موقعیت شما داریم .", Toast.LENGTH_LONG).show();
            final LocationManager manager = (LocationManager) ContextModel.getContext().getSystemService(Context.LOCATION_SERVICE);
            String provider = Settings.Secure.getString(ContextModel.getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!provider.contains("gps")) { //if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                ContextModel.getContext().sendBroadcast(poke);
            }
        } catch (Exception e) {

        }
    }

    private void ButtonsClickListener() {
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeasonMenu.class));
            }
        });


        ImageView bookmarks = (ImageView) findViewById(R.id.fav);
        bookmarks.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                intenthappen = true;
                Intent intent = new Intent(MainActivity.this, Favorites.class);
                startActivity(intent);
            }

        });

        if (!App.FavList) {
            bookmarks.setVisibility(View.GONE);
        }

        ImageView about = (ImageView) findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                intenthappen = true;
                Intent i5 = new Intent(MainActivity.this, ShowContent.class);
                i5.putExtra("aboutus", true);
                startActivity(i5);
            }
        });


        ImageView settings = (ImageView) findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                intenthappen = true;
                Intent intent = new Intent(MainActivity.this, Settings_Activity.class);
                startActivity(intent);

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intenthappen = true;
                Intent i5 = new Intent(MainActivity.this, Search_all.class);
                startActivity(i5);
            }
        });

        ImageView rate = (ImageView) findViewById(R.id.rate);
        rate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try {


                    intenthappen = true;
                    Uri uri = Uri.parse("bazaar://details?id=" + BuildConfig.APPLICATION_ID);
                    Intent myAppLinkToMarket = new Intent(Intent.ACTION_EDIT, uri);
                    startActivity(myAppLinkToMarket);


                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "بازار را نصب ندارید؟", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void DrawerBuilder() {

        rtlNavigationView = (RtlNavigationView) findViewById(R.id.nav_view);
        rtlNavigationView.setNavigationItemSelectedListener(this);

        rtlNavigationView.inflateRtlMenu(MainActivity.this, R.menu.activity_main_drawer);
        View v = rtlNavigationView.inflateHeaderView(R.layout.nav_header_main);

        if (!App.NavigationDrawer) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            opener.setVisibility(View.INVISIBLE);
        }

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                isDrawerOpened = true;
                App.Loger("onDrawerOpened");
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                isDrawerOpened = false;
                App.Loger("onDrawerClosed");
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        opener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDrawerOpened) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });

    }

    private void InitViews() {

        opener = (ImageView) findViewById(R.id.opener);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        vazirb = Typeface.createFromAsset(getAssets(), "fonts/vazirb.ttf");
        vazir = Typeface.createFromAsset(getAssets(), "fonts/vazir.ttf");
        header = (ImageView) findViewById(R.id.header);
        search = (ImageView) findViewById(R.id.search);
        ratel = (LinearLayout) findViewById(R.id.ratel);
        start = (Button) findViewById(R.id.enterbtn);

    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        if (isDrawerOpened) {
            drawer.closeDrawer(Gravity.RIGHT, true);
        } else {
            if (App.DoubleTapToExit) {

                if (doubleBackToExitPressedOnce) {
                    if (Build.VERSION.SDK_INT >= 16) {
                        finishAffinity();
                        stopService(new Intent(MainActivity.this, BackgroundSoundService.class));
                    } else {
                        finish();
                        stopService(new Intent(MainActivity.this, BackgroundSoundService.class));
                    }
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getResources().getString(R.string.exittoast), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);

            } else {

                doubleBackToExitPressedOnce = false;
                AlertDialog.Builder alert;

                if (!Settings_Activity.loadNightMode(this)) {
                    alert = new AlertDialog.Builder(this, 5);
                } else {
                    alert = new AlertDialog.Builder(this, 4);
                }

                alert.setMessage("تایید خروج؟");
                alert.setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= 16) {
                            finishAffinity();
                            stopService(new Intent(MainActivity.this, BackgroundSoundService.class));
                        } else {
                            finish();
                            stopService(new Intent(MainActivity.this, BackgroundSoundService.class));
                        }


                    }

                });
                alert.setNegativeButton("خیر", null);
                alert.show();
            }
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        AlarmsManager.getInstance().setAlarm();
        App.HandlerPauseBackgroundMusic(intenthappen, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationsManager.getInstans().startStep1();
        int theme2;
        if (!Settings_Activity.loadNightMode(this)) {
            theme2 = 0;
        } else {
            theme2 = 1;
        }

        if (theme != theme2) {
            MainActivity.this.recreate();
        }

        App.HandlerResumeBackgroundMusic(this);
    }


    @Override
    public void onDestroy() {
        AlarmsManager.getInstance().setAlarm();
        LocationsManager.getInstans().startStep1();
        super.onDestroy();
    }



    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                LocationsManager.getInstans().startStep3();

            } else {

            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.favnav) {
            intenthappen = true;
            Intent i5 = new Intent(MainActivity.this, Favorites.class);
            startActivity(i5);
        } else if (id == R.id.searchnav) {

            intenthappen = true;
            Intent i5 = new Intent(MainActivity.this, Search_all.class);
            startActivity(i5);

        } else if (id == R.id.aboutnav) {
            intenthappen = true;
            Intent i5 = new Intent(MainActivity.this, ShowContent.class);
            i5.putExtra("aboutus", true);
            startActivity(i5);
        }

        drawer.closeDrawer(Gravity.RIGHT);

        return true;
    }

}

