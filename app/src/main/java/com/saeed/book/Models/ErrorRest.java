package com.saeed.book.Models;

/**
 * Developed by javid
 * Project : numberGenerator
 */
public class ErrorRest {
    public static final String ERROR_MESSAGE = "error.message";
    private String value;
    private String statue;

    public ErrorRest(String value, String statue) {
        this.value = value;
        this.statue = statue;
    }
}
