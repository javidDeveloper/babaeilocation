package com.saeed.book.Tools;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.saeed.book.Managers.AlarmsManager;
import com.saeed.book.Managers.LocationsManager;
import com.saeed.book.Managers.NetworkManager;
import com.saeed.book.Models.ContextModel;
import com.saeed.book.Models.LoctionStatus;
import com.saeed.book.Models.StatusRest;

import java.util.Observable;
import java.util.Observer;

/**
 * Developed by javid
 * Project : book
 */
public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, Observer {
    private static final String TAG = LocationService.class.getSimpleName();
    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest = new LocationRequest();
    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() + "LocationBroadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    private LoctionStatus loctionStatus = new LoctionStatus();
    private TelephonyManager telephonyManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocationsManager.getInstans().addObserver(this);
        statusCheck();
        AlarmsManager.getInstance().setAlarm();
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
//        mLocationRequest.setInterval(3000);
//        mLocationRequest.setFastestInterval(3000);

        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes
        mLocationRequest.setPriority(priority);
        mLocationClient.connect();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
     * LOCATION CALLBACKS
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "== Error On onConnected() Permission not granted");
            ActivityCompat.requestPermissions(ContextModel.getCurrentActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE,},
                    1500);

            return;
        }
        String imei = telephonyManager.getDeviceId();
        loctionStatus.setImeiCode(imei);
        if (mLocationClient.isConnected() && mLocationClient != null && mLocationRequest != null)
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        Log.d(TAG, "Connected to Google API");
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
//        Log.d(TAG, "Connection suspended");
//        Toast.makeText(this, "Connection suspended", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.d(TAG, "Location changed");
        loctionStatus.setLatitude(String.valueOf(location.getLatitude()));
        loctionStatus.setLongitude(String.valueOf(location.getLongitude()));
        if (location != null) {
            Log.d(TAG, "== location != null");
            sendMessageToUI(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
        NetworkManager.getInstance().callLocation(loctionStatus);
    }

    private void sendMessageToUI(String lat, String lng) {
        Log.d("sendMessageToUI _: ", lat + lng);
        loctionStatus.setLatitude(lat);
        loctionStatus.setLongitude(lng);
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        NetworkManager.getInstance().callLocation(loctionStatus);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof LocationsManager) {
            if (arg instanceof StatusRest) {

            }
        }
    }
}
