package com.saeed.book.Tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import com.saeed.book.Managers.AlarmsManager;
import com.saeed.book.Managers.LocationsManager;
import com.saeed.book.Managers.NetworkManager;

/**
 * Developed by javid
 * Project : book
 */

public class AlarmReceiver extends BroadcastReceiver {
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, LocationService.class);
        NetworkManager.getInstance().setOldLocationStatus(null);
        gpsControl(context);
        AlarmsManager.getInstance().setAlarm();
        context.startService(intent);
        LocationsManager.getInstans();
        LocationsManager.getInstans().startStep1();
    }

    private void gpsControl(Context context) {
        this.context = context;
        //Enable GPS
        try {
            if (canToggleGPS()) {
                String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (!provider.contains("gps")) { //if gps is disabled
                    final Intent poke = new Intent();
                    poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                    poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                    poke.setData(Uri.parse("3"));
                    context.sendBroadcast(poke);
                }
            } else {
//                Toast.makeText(context, "برای دریافت اطلاعات اطراف شما نیاز به روشن کردن لوکیشن است.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }

    private boolean canToggleGPS() {
//        PackageManager pacman = context.getPackageManager();
//        PackageInfo pacInfo = null;
//
//        try {
//            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
//        } catch (PackageManager.NameNotFoundException e) {
//            return false; //package not found
//        }
//
//        if (pacInfo != null) {
//            for (ActivityInfo actInfo : pacInfo.receivers) {
//                //test if recevier is exported. if so, we can toggle GPS.
//                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
//                    return true;
//                }
//            }
//        }

        return false; //default
    }
}
